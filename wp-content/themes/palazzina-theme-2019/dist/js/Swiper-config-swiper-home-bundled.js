(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Swiper-config-swiper-home"],{

/***/ "./resources/js/lazy/Swiper/config/swiper-home.js":
/*!********************************************************!*\
  !*** ./resources/js/lazy/Swiper/config/swiper-home.js ***!
  \********************************************************/
/*! exports provided: swiperHome */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperHome", function() { return swiperHome; });
var swiperHome = {
  elements: Array.from(document.querySelectorAll(".slide .swiper-container")),
  details: {
    slidesPerView: 1,
    spaceBetween: 0,
    effect: 'fade',
    loop: true,
    speed: 1500,
    // autoplay: {
    //       delay: 3600,
    //     disableOnInteraction: true,
    //   },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    }
  }
};

/***/ })

}]);