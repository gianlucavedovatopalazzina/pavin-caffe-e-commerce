/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"/main": 0
/******/ 	};
/******/
/******/
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "dist/js/" + ({"Swiper-config-swiper-carousel":"Swiper-config-swiper-carousel","vendors-PhotoSwipe":"vendors-PhotoSwipe","PhotoSwipe":"PhotoSwipe","vendors-Swiper-Swiper":"vendors-Swiper-Swiper","Swiper-Swiper":"Swiper-Swiper"}[chunkId]||chunkId) + "-bundled.js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/wp-content/themes/palazzina-theme-2019/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/globals/Header.js":
/*!****************************************!*\
  !*** ./resources/js/globals/Header.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Header = function Header() {
  _classCallCheck(this, Header);
};

jQuery(".hamb").click(function () {
  jQuery(".header.mobile").toggleClass("open");
});
jQuery(".header.mobile .header__menu li a").click(function () {
  jQuery(".header.mobile").toggleClass("open");
});
/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./resources/js/globals/index.js":
/*!***************************************!*\
  !*** ./resources/js/globals/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Header */ "./resources/js/globals/Header.js");

/* harmony default export */ __webpack_exports__["default"] = ([_Header__WEBPACK_IMPORTED_MODULE_0__["default"]]);

/***/ }),

/***/ "./resources/js/helpers/disableScroll.js":
/*!***********************************************!*\
  !*** ./resources/js/helpers/disableScroll.js ***!
  \***********************************************/
/*! exports provided: enableScroll, disableScroll */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enableScroll", function() { return enableScroll; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "disableScroll", function() { return disableScroll; });
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {
  37: 1,
  38: 1,
  39: 1,
  40: 1
};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault) e.preventDefault();
  e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

function disableScroll() {
  if (window.addEventListener) // older FF
    window.addEventListener('DOMMouseScroll', preventDefault, false);
  document.addEventListener('wheel', preventDefault, {
    passive: false
  }); // Disable scrolling in Chrome

  window.onwheel = preventDefault; // modern standard

  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE

  window.ontouchmove = preventDefault; // mobile

  document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
  if (window.removeEventListener) window.removeEventListener('DOMMouseScroll', preventDefault, false);
  document.removeEventListener('wheel', preventDefault, {
    passive: false
  }); // Enable scrolling in Chrome

  window.onmousewheel = document.onmousewheel = null;
  window.onwheel = null;
  window.ontouchmove = null;
  document.onkeydown = null;
}



/***/ }),

/***/ "./resources/js/helpers/getHeight.js":
/*!*******************************************!*\
  !*** ./resources/js/helpers/getHeight.js ***!
  \*******************************************/
/*! exports provided: getHeight */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHeight", function() { return getHeight; });
var getHeight = function getHeight(target, className) {
  var cont;

  if (!className) {
    cont = target;
  } else {
    cont = target.querySelector(className);
  } // console.log(cont);


  var cloned_cont = cont.cloneNode(true);
  cloned_cont.style.width = cont.clientWidth + 'px';
  cloned_cont.style.height = "auto";
  document.body.appendChild(cloned_cont);
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve(cloned_cont.clientHeight);
      document.body.removeChild(cloned_cont);
    }, 100);
  });
};

/***/ }),

/***/ "./resources/js/lazy lazy recursive ^\\.\\/.*$":
/*!**********************************************************!*\
  !*** ./resources/js/lazy lazy ^\.\/.*$ namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./PhotoSwipe": [
		"./resources/js/lazy/PhotoSwipe.js",
		"vendors-PhotoSwipe",
		"PhotoSwipe"
	],
	"./PhotoSwipe.js": [
		"./resources/js/lazy/PhotoSwipe.js",
		"vendors-PhotoSwipe",
		"PhotoSwipe"
	],
	"./Swiper/Swiper": [
		"./resources/js/lazy/Swiper/Swiper.js",
		"vendors-Swiper-Swiper",
		"Swiper-Swiper"
	],
	"./Swiper/Swiper.js": [
		"./resources/js/lazy/Swiper/Swiper.js",
		"vendors-Swiper-Swiper",
		"Swiper-Swiper"
	],
	"./Swiper/config/swiper-carousel": [
		"./resources/js/lazy/Swiper/config/swiper-carousel.js",
		"Swiper-config-swiper-carousel"
	],
	"./Swiper/config/swiper-carousel.js": [
		"./resources/js/lazy/Swiper/config/swiper-carousel.js",
		"Swiper-config-swiper-carousel"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./resources/js/lazy lazy recursive ^\\.\\/.*$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_Router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/Router */ "./resources/js/utils/Router.js");
/* harmony import */ var _scripts_Home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scripts/Home */ "./resources/js/scripts/Home.js");
/* harmony import */ var _scripts_Pages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scripts/Pages */ "./resources/js/scripts/Pages.js");
/* harmony import */ var _scripts_Termini__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scripts/Termini */ "./resources/js/scripts/Termini.js");
/* harmony import */ var _scripts_Toggle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./scripts/Toggle */ "./resources/js/scripts/Toggle.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./globals */ "./resources/js/globals/index.js");
/* harmony import */ var _scripts_Single__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./scripts/Single */ "./resources/js/scripts/Single.js");
/* harmony import */ var _scripts_Single__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_scripts_Single__WEBPACK_IMPORTED_MODULE_6__);
// import $ from "jquery";
// window.$ = window.jQuery = $;




 //Globals scripts


window.toggle = _scripts_Toggle__WEBPACK_IMPORTED_MODULE_4__["default"];
Object(_scripts_Toggle__WEBPACK_IMPORTED_MODULE_4__["default"])();
_globals__WEBPACK_IMPORTED_MODULE_5__["default"].forEach(function (cls) {
  return new cls();
});
new _utils_Router__WEBPACK_IMPORTED_MODULE_0__["default"]([{
  attribute: ".swiper-container",
  instance: 'Swiper/Swiper.js',
  fn: function fn(_ref) {
    var Swiper = _ref["default"];
    Swiper.init();
  }
}, {
  attribute: ".photoswipe",
  instance: 'PhotoSwipe.js',
  fn: function fn(_ref2) {
    var PhotoSwipe = _ref2["default"];
    PhotoSwipe.initPhotoSwipeFromDOM(".photoswipe");
  }
}, {
  attribute: ".home",
  instance: _scripts_Home__WEBPACK_IMPORTED_MODULE_1__["default"]
}, {
  attribute: ".header",
  instance: _scripts_Pages__WEBPACK_IMPORTED_MODULE_2__["default"]
}, {
  attribute: ".single",
  instance: _scripts_Single__WEBPACK_IMPORTED_MODULE_6___default.a
}, {
  attribute: ".page-termini",
  instance: _scripts_Termini__WEBPACK_IMPORTED_MODULE_3__["default"]
}]).watch();


/***/ }),

/***/ "./resources/js/scripts/Home.js":
/*!**************************************!*\
  !*** ./resources/js/scripts/Home.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Home = function Home() {
  _classCallCheck(this, Home);

  console.log("class Home");
  document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();
      document.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth'
      });
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./resources/js/scripts/Pages.js":
/*!***************************************!*\
  !*** ./resources/js/scripts/Pages.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Pages = function Pages() {
  _classCallCheck(this, Pages);

  console.log("class Pages"); // animation header 

  jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 150) {
      jQuery('.header').addClass('active');
    } else {
      jQuery('.header').removeClass('active');
    }
  }); // animation pages  

  (function ($) {
    jQuery.fn.visible = function (partial) {
      var $t = jQuery(this),
          $w = jQuery(window),
          viewTop = $w.scrollTop(),
          viewBottom = viewTop + $w.height(),
          _top = $t.offset().top,
          _bottom = _top + $t.height(),
          compareTop = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;

      return compareBottom <= viewBottom && compareTop >= viewTop;
    };
  })(jQuery);

  jQuery(window).scroll(function (event) {
    // Block add class when is visible
    jQuery(".block-animation").each(function (i, el) {
      var el = jQuery(el);

      if (el.visible(true)) {
        el.addClass("active");
      } else {
        el.removeClass("active");
      }
    });
    jQuery(".img-left , .img-right").each(function (i, el) {
      var el = jQuery(el);

      if (el.visible(true)) {
        el.addClass("active");
      } else {// el.removeClass("active"); 
      }
    });
  }); // Page loaded

  jQuery(window).load(function () {
    jQuery(".load").addClass("loaded");
    jQuery("body").addClass("loaded");
  }); // pavin code

  jQuery('.return-top').on("click", function () {
    jQuery('html, body').animate({
      scrollTop: 0
    }, 600);
  });
  jQuery(window).scroll(function () {
    var scroll_pos = jQuery(document).scrollTop();
    var calc_scroll_pos = scroll_pos * 0.19;
    var calc_scroll_pos2 = scroll_pos * 0.16;
    var calc_scroll_rot1 = scroll_pos * 0.09;
    var calc_scroll_rot2 = scroll_pos * 0.12;
    var calc_scroll_rot3 = scroll_pos * 0.14;
    jQuery('.parallax-animation1').attr('style', 'transform: translateY(' + calc_scroll_pos + 'px) rotate(' + calc_scroll_rot1 + 'deg); ; transition: transform 0s linear 0s; will-change: transform');
    jQuery('.parallax-animation2').attr('style', 'transform: translateY(' + calc_scroll_pos2 + 'px) rotate(' + -calc_scroll_rot2 + 'deg); transition: transform 0s linear 0s; will-change: transform');
    jQuery('.parallax-animation3').attr('style', 'transform: translateY(' + calc_scroll_pos2 + 'px) rotate(' + -calc_scroll_rot3 + 'deg); transition: transform 0s linear 0s; will-change: transform'); //effetto scroll row

    jQuery('.fade-in-load-scroll').each(function () {
      var element = jQuery(this);
      setTimeout(function () {
        top_load_element(element);
      }, 250, element);
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Pages);

/***/ }),

/***/ "./resources/js/scripts/Single.js":
/*!****************************************!*\
  !*** ./resources/js/scripts/Single.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./resources/js/scripts/Termini.js":
/*!*****************************************!*\
  !*** ./resources/js/scripts/Termini.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Termini = function Termini() {
  _classCallCheck(this, Termini);

  console.log("class Termini");
  jQuery("#pagamenti").ready(function (i) {
    jQuery(".text-pagamenti").toggleClass("open");
  });
  jQuery("#pagamenti").click(function (i) {
    jQuery(".block-text").removeClass("open");
    jQuery(".text-pagamenti").toggleClass("open");
  });
  jQuery("#spedizioni").click(function (i) {
    jQuery(".block-text").removeClass("open");
    jQuery(".text-spedizioni").toggleClass("open");
  });
  jQuery("#termini").click(function (i) {
    jQuery(".block-text").removeClass("open");
    jQuery(".text-termini").toggleClass("open");
  }); // jQuery(".cat_fin_all .prod_finiture").ready(function() {
  //     jQuery(".cat_fin_all .prod_finiture").addClass("load");
  // });

  jQuery(".block-terms-mobile").click(function () {
    jQuery(this).each(function () {
      jQuery(this).toggleClass("open");
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Termini);

/***/ }),

/***/ "./resources/js/scripts/Toggle.js":
/*!****************************************!*\
  !*** ./resources/js/scripts/Toggle.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_disableScroll__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../helpers/disableScroll */ "./resources/js/helpers/disableScroll.js");
/* harmony import */ var _helpers_getHeight__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../helpers/getHeight */ "./resources/js/helpers/getHeight.js");



var getParent = function getParent(el, parentClassname) {
  var classes = Array.from(el.parentElement.classList);

  if (classes.includes(parentClassname)) {
    return el.parentElement;
  }

  return getParent(el.parentElement, parentClassname);
};

var handleLisClick = function handleLisClick(_ref) {
  var target = _ref.target;
  var button = target.closest('.toggle__box-btn');

  if (button.classList.contains('btn-disable-scroll')) {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    Object(_helpers_disableScroll__WEBPACK_IMPORTED_MODULE_0__["disableScroll"])();
  }

  try {
    var toggle__box = getParent(button, 'toggle__box');
    toggle__box.classList.toggle('toggle__box--open');
  } catch (e) {
    console.log(e);
  }
};

var closeToggle = function closeToggle(_ref2) {
  var target = _ref2.target;
  var button = target.closest('.toggle__box-close');
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth'
  });
  Object(_helpers_disableScroll__WEBPACK_IMPORTED_MODULE_0__["enableScroll"])();

  try {
    var toggle__box = getParent(button, 'toggle__box');
    toggle__box.classList.remove('toggle__box--open');
  } catch (e) {
    console.log(e);
  }
};

var slideToggleClick = function slideToggleClick(_ref3) {
  var target = _ref3.target;
  var button = target.closest('.toggle__box-btn--slide_toggle');

  try {
    var toggle__box = getParent(button, 'toggle__box');
    var toggle__box_container = toggle__box.querySelector('.toggle__box-container');
    toggle__box.classList.toggle('toggle__box--open');

    if (toggle__box.classList.contains("toggle__box--open")) {
      var height;
      Object(_helpers_getHeight__WEBPACK_IMPORTED_MODULE_1__["getHeight"])(toggle__box_container).then(function (payload) {
        height = payload;
        toggle__box_container.style.height = "".concat(height, "px");
        button.classList.add('hidden');
      });
      return;
    }

    toggle__box_container.style.height = "0px";
  } catch (e) {
    console.log(e);
  } // const box = target.closest('.toggle__box-btn--slide_toggle');
  // const cont = box.querySelector('.toggle__box-cont');
  // box.classList.toggle('toggle__box--open');
  // if (box.classList.contains("toggle__box--open")) {
  //   let height;
  //   getHeight(box, ".toggle__box-cont")
  //     .then(payload => {
  //       height = payload;
  //       cont.style.height = `${ height }px`;
  //     });
  //   return;
  // }
  // cont.style.height = `0px`;

};

/* harmony default export */ __webpack_exports__["default"] = (function () {
  var btns_slide = document.querySelectorAll(".toggle__box-btn--slide_toggle");
  btns_slide.forEach(function (btn) {
    return btn.addEventListener("click", slideToggleClick);
  });
  var btns = document.querySelectorAll(".toggle__box-btn");
  btns.forEach(function (btn) {
    return btn.addEventListener("click", handleLisClick);
  });
  var closes = document.querySelectorAll(".toggle__box-close");
  closes.forEach(function (close) {
    return close.addEventListener("click", closeToggle);
  });
});

/***/ }),

/***/ "./resources/js/utils/Router.js":
/*!**************************************!*\
  !*** ./resources/js/utils/Router.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Router =
/*#__PURE__*/
function () {
  function Router(routes) {
    _classCallCheck(this, Router);

    Object.assign(this, {
      routes: routes
    });
  }
  /**
   * @param {string} attribute Html Classname to check
   * @param {class|string} instance Class instance or path to class
   * @param {function} [fn] Function to pass for tweak the module 
   */


  _createClass(Router, [{
    key: "watch",
    value: function watch() {
      var _this = this;

      this.routes.forEach(function (_ref) {
        var attribute = _ref.attribute,
            instance = _ref.instance,
            fn = _ref.fn;
        if (!document.querySelector(attribute)) return;

        if (typeof instance === "string") {
          _this.lazy(instance, fn);
        } else {
          fn ? fn(instance) : new instance();
        }
      });
    }
    /**
     * Lazy load module
     * @param {string} path Path where to find the module
     * @param {function} [fn] Custom function to trigger passing module instance
     * @return {void}
     */

  }, {
    key: "lazy",
    value: function lazy(path, fn) {
      /**
       * Default, class imported dynamically
       * @param {class} default
       */
      var def = function def(_ref2) {
        var _ = _ref2["default"];
        return new _();
      };

      fn = fn ? fn : def;
      __webpack_require__("./resources/js/lazy lazy recursive ^\\.\\/.*$")("./".concat(path)).then(fn);
    }
  }]);

  return Router;
}();

/* harmony default export */ __webpack_exports__["default"] = (Router);

/***/ }),

/***/ "./resources/sass/main.scss":
/*!**********************************!*\
  !*** ./resources/sass/main.scss ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************!*\
  !*** multi ./resources/js/main.js ./resources/sass/main.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/js/main.js */"./resources/js/main.js");
module.exports = __webpack_require__(/*! /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/sass/main.scss */"./resources/sass/main.scss");


/***/ })

/******/ });