(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Swiper-Swiper"],{

/***/ "./resources/js/lazy/Swiper/Swiper.js":
/*!********************************************!*\
  !*** ./resources/js/lazy/Swiper/Swiper.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/dist/js/swiper.esm.bundle.js");
/* harmony import */ var _config_swiper_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config/swiper-carousel */ "./resources/js/lazy/Swiper/config/swiper-carousel.js");
 //Swiper configuration 



var configs = [_config_swiper_carousel__WEBPACK_IMPORTED_MODULE_1__["swiperCarousel"], _config_swiper_carousel__WEBPACK_IMPORTED_MODULE_1__["swiperCarousel2"]];
/**
 * Init function for building all slides base on 
 * className and configuration details
 */

swiper__WEBPACK_IMPORTED_MODULE_0__["default"].init = function () {
  configs.forEach(function (_ref) {
    var elements = _ref.elements,
        details = _ref.details;
    elements.forEach(function (el) {
      var swiper = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](el, details);

      if (details.init == false) {
        swiper.init();
      }
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (swiper__WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./resources/js/lazy/Swiper/config/swiper-carousel.js":
/*!************************************************************!*\
  !*** ./resources/js/lazy/Swiper/config/swiper-carousel.js ***!
  \************************************************************/
/*! exports provided: swiperCarousel, swiperCarousel2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperCarousel", function() { return swiperCarousel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperCarousel2", function() { return swiperCarousel2; });
var swiperCarousel = {
  elements: Array.from(document.querySelectorAll(".swiper-container.slider-category")),
  details: {
    speed: 500,
    // init: false,
    slidesPerView: 4,
    spaceBetween: 15,
    threshold: 20,
    navigation: {
      nextEl: '.swiper-next',
      prevEl: '.swiper-prev'
    },
    // centerInsufficientSlides : true,
    // slideToClickedSlide: true,
    preventClicksPropagation: false,
    preventClicks: false,
    breakpoints: {
      767: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      769: {
        slidesPerView: 3,
        spaceBetween: 40
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },
    on: {
      init: function init() {
        if (this.slides.length < 5) {
          document.querySelector(swiperCarousel.details.navigation.nextEl).style.display = "none";
          document.querySelector(swiperCarousel.details.navigation.prevEl).style.display = "none";
        }
      }
    }
  }
};
var swiperCarousel2 = {
  elements: Array.from(document.querySelectorAll(".swiper-container.slider-mobile")),
  details: {
    speed: 400,
    init: false,
    slidesPerView: 1,
    spaceBetween: 0,
    threshold: 20,
    // navigation: {
    //     nextEl: '.swiper-next',
    //     prevEl: '.swiper-prev',
    // },
    on: {// init() {
      //     if (this.slides.length < 5) {
      //         document.querySelector(swiperCarousel.details.navigation.nextEl).style.display = "none";
      //         document.querySelector(swiperCarousel.details.navigation.prevEl).style.display = "none";
      //     }
      // }
    },
    breakpoints: {
      // when window width is <= 480px     
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      // when window width is <= 640px     
      640: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      770: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  }
};

/***/ })

}]);