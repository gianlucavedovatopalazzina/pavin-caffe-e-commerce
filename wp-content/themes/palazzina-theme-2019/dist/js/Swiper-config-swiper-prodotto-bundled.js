(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Swiper-config-swiper-prodotto"],{

/***/ "./resources/js/lazy/Swiper/config/swiper-prodotto.js":
/*!************************************************************!*\
  !*** ./resources/js/lazy/Swiper/config/swiper-prodotto.js ***!
  \************************************************************/
/*! exports provided: swiperProdotto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperProdotto", function() { return swiperProdotto; });
var swiperProdotto = {
  elements: Array.from(document.querySelectorAll(".slide--prodotto")),
  details: {
    speed: 400,
    init: false,
    slidesPerView: '1',
    spaceBetween: 0,
    pagination: {
      el: '.swiper-pagination',
      clickable: true // renderBullet: function (index, className) {
      //     return '<span class="' + className + '">' + (index + 1) + '</span>';
      // },

    },
    on: {
      init: function init() {
        changeSlide(this);
      },
      slideChange: function slideChange() {
        thumbActive(this);
      }
    }
  }
};

var changeSlide = function changeSlide(swiper) {
  var thumbnails = document.querySelectorAll(".pagination--thumbnail .thumb");
  thumbnails[0].classList.add('thumb--active');
  thumbnails.forEach(function (thumb, index) {
    thumb.addEventListener("click", function () {
      swiper.slideTo(index);
    });
  });
};

var thumbActive = function thumbActive(swiper) {
  var thumbnails = document.querySelectorAll(".pagination--thumbnail .thumb");
  var thumb_active = document.querySelector(".pagination--thumbnail .thumb--active");
  thumb_active.classList.remove('thumb--active');
  thumbnails[swiper.realIndex].classList.add('thumb--active');
};

/***/ })

}]);