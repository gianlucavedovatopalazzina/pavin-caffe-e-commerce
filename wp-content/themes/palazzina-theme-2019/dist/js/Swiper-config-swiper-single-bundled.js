(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Swiper-config-swiper-single"],{

/***/ "./resources/js/lazy/Swiper/config/swiper-single.js":
/*!**********************************************************!*\
  !*** ./resources/js/lazy/Swiper/config/swiper-single.js ***!
  \**********************************************************/
/*! exports provided: swiperSingle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperSingle", function() { return swiperSingle; });
var swiperSingle = {
  elements: Array.from(document.querySelectorAll(".slide--single")),
  details: {
    speed: 800,
    init: false,
    slidesPerView: '1',
    spaceBetween: 0,
    loop: true,
    autoplay: {
      delay: 2500
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    }
  }
};

/***/ })

}]);