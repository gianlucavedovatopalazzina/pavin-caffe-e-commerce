(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Swiper-config-swiper-full"],{

/***/ "./resources/js/lazy/Swiper/config/swiper-full.js":
/*!********************************************************!*\
  !*** ./resources/js/lazy/Swiper/config/swiper-full.js ***!
  \********************************************************/
/*! exports provided: swiperFull */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swiperFull", function() { return swiperFull; });
var swiperFull = {
  elements: Array.from(document.querySelectorAll(".slide--full")),
  details: {
    speed: 400,
    slidesPerView: 'auto',
    spaceBetween: 30,
    centerInsufficientSlides: true
  }
};

/***/ })

}]);