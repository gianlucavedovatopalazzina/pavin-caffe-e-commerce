

<?php
$pagamentiID = pll__('116');
$spedizioniID = pll__('118');
$terminiID = pll__('114');

// excerpt 
$pageID = get_the_ID();
$page_object = get_page( $pageID );
$text = $page_object->post_content;
$start = strpos($text, '<p>');
$end = strpos($text, '</p>', $start);
$excerpt = substr($text, $start, $end-$start+3);

?>

<div class="page-main termini page-termini">
    <div class="page__content">
        <div>
        <?php if(!wp_is_mobile() ): ?> 
            <div class="container-l">
                <div class="flex">
                    <div class="nav">
                        <div id="pagamenti"><?php echo e(get_the_title($pagamentiID)); ?></div>
                        <div id="spedizioni"><?php echo e(get_the_title($spedizioniID)); ?></div>
                        <div id="termini"><?php echo e(get_the_title($terminiID)); ?></div>
                    </div>
                    <div class="cont-text">
                        <div class="text-pagamenti block-text">
                            <h2 class="title-xxl"><?php echo e(get_the_title($pagamentiID)); ?></h2>

                            <?php
                            $page_object = get_page( $pagamentiID );
                            echo $page_object->post_content;
                            ?>

                        </div>
                        <div class="text-spedizioni block-text">
                            <h2 class="title-xxl"><?php echo e(get_the_title($spedizioniID)); ?></h2>
                            <?php
                            $page_object = get_page( $spedizioniID );
                            echo $page_object->post_content;
                            ?>
                        </div>
                        <div class="text-termini block-text">
                            <h2 class="title-xxl"><?php echo e(get_the_title($terminiID)); ?></h2>
                            <?php
                            $page_object = get_page( $terminiID );
                            echo $page_object->post_content;
                            ?>
                        </div>
                         
                    </div>
                </div>
            </div>
            <?php elseif(wp_is_mobile() ): ?> 
            <div class="container-l mobile">
                <div class="">
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl"><?php echo e(get_the_title($pagamentiID)); ?></h2>
                        <div class="excerpt">
                            <?php
                            $page_object = get_page( $pagamentiID);
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            ?>
                        </div>
                        <div class="discover">
                            <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                        </div>
                    </div>
                        <div class="full-text">
                            <div class="discover">
                                <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(-90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                            </div>
                            <?php echo $text ?>
                        </div>
                    </div>
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl"><?php echo e(get_the_title($spedizioniID)); ?></h2>
                        <div class="excerpt">
                            <?php
                            $page_object = get_page( $spedizioniID );
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            ?>
                        </div>
                        <div class="discover">
                            <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                        </div>
                    </div>
            
             
                        <div class="full-text">
                            <div class="discover">
                                <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(-90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                            </div>
                            <?php echo $text ?>
                        </div>
                    </div>
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl"><?php echo e(get_the_title($terminiID)); ?></h2>
                        <div class="excerpt">
                            <?php
                            $page_object = get_page( $terminiID );
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            ?>
                         </div>
                         <div class="discover">
                                <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                            </div>
                        </div>

                         <div class="full-text">
                            <div class="discover">
                                <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25px" height="14px" viewBox="0 0 25 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line Copy 3</title>
    <desc>Created with Sketch.</desc>
    <g id="Mobile-/-Shop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="iPhone-8-Copy-2" transform="translate(-324.000000, -204.000000)" stroke="#250F02" stroke-width="1.5">
            <polyline id="Line-Copy-3" transform="translate(336.500002, 210.550442) scale(1, -1) rotate(-90.000000) translate(-336.500002, -210.550442) " points="342.000002 199.550442 331.000002 210.550442 342.000002 221.550442"/>
        </g>
    </g>
</svg>'; ?>
                            </div>
                            <?php echo $text ?>
                         </div>
                    </div> 
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/partials/content/content-termini.blade.php ENDPATH**/ ?>