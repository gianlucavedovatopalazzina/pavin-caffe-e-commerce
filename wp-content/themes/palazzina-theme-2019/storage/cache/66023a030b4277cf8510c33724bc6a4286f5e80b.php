<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );

?>



<?php $__env->startSection('content'); ?>

<div class="head-category" style="background-image: url(<?php echo e($thumb['0']); ?>)">
		<h1 class="woocommerce-products-header__title page-title"><?php echo e(woocommerce_page_title()); ?></h1>
</div>

<?php echo $__env->make('woocommerce.content.content-category', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('partials.components.return-top', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('partials.components.pre-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views//woocommerce/archive-product.blade.php ENDPATH**/ ?>