<?php
$categoryid = '16';
$categoryname = 'product_cat';
global $post;
?>

<?php if(is_front_page()): ?> 
<?php
$bloccocategoria = get_field('blocco_categoria_correlata', $categoryname . '_' . $categoryid );

$linkcateg = get_category_link( $categoryid );
?>

<?php elseif(is_singular( 'product' )): ?>

<?php
$categ = get_the_terms( $post->ID, 'product_cat' );

$categparentID = join(', ', wp_list_pluck($categ, 'parent'));

$bloccocategoria = get_field('blocco_categoria_correlata', $categoryname . '_' . $categparentID );

$linkcateg = get_category_link( $categparentID );

?>

<?php else: ?> 

<?php

$term = get_queried_object();

// var_dump($term);

$categoryname = 'product_cat';

$bloccocategoria = get_field('blocco_categoria_correlata', $term );

$linkcateg = get_category_link( $term );

?>

<?php endif; ?>

<?php if($bloccocategoria): ?> 

<div class="category-related block-animation">
    <div class="container-l">
        <a href="<?php echo e($linkcateg); ?>">
            <div class="block-overflow">
                <div class="cont-related-cat" style="background-image: url(<?php echo e(esc_url( $bloccocategoria['cover_image']['url'] )); ?>)">
                    <div class="cont-text">
                        <h5 class="title-xs"><?php echo e($bloccocategoria['sottotitolo-cat']); ?></h5>
                        <h4 class="title-xxl"><?php echo e($bloccocategoria['titolo-cat']); ?></h4>
                        <p><?php echo e($bloccocategoria['testo-cat']); ?></p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<?php endif; ?><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/woocommerce/content/category-related.blade.php ENDPATH**/ ?>