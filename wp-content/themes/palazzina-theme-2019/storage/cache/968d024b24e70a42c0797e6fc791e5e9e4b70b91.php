<?php 

$cateID = 16;

?>

<?php $__env->startSection('content'); ?>
    <?php while( have_posts() ): ?> <?php the_post() ?>
    <header class="woocommerce-products-header">
        <div class="head-category" style="background-image: url(<?php echo e(get_the_post_thumbnail_url()); ?>)">
            <h1 class="woocommerce-products-header__title page-title"><?php echo e(get_the_title()); ?></h1>
        </div>
    </header>

    <div class="content-archive">
            <div class="container">
            <?php
    
            $terms = get_terms( array(
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
                'child_of' => $cateID, 
            ) );
            // var_dump($terms);
            ?>
        
            <?php $__currentLoopData = $terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
        
            <?php
            //  var_dump($term);
            // $svg1 = get_field('svg_1', $term->term_taxonomy_id );

            $svg1 = get_field('svg_1', $term->taxonomy . '_' . $term->term_id );

            $svg2 = get_field('svg_2', $term->taxonomy . '_' . $term->term_id );
            ?>
        
            <?php if( $svg1 ): ?> 
            <div class="img-left">
                <img src="<?php echo e($svg1['url']); ?>">
            </div>
            <?php endif; ?>

            <?php if( $svg2 ): ?> 
            <div class="img-right">
                <img src="<?php echo e($svg2['url']); ?>">
            </div>
            <?php endif; ?>
         
                <div class="single-cat" id="link-<?php echo e($term->slug); ?>">
                    <div class="cont-tit">
                        <h3 class="title-xs"><?php echo e($term->description); ?></h3>
                        <h2 class="title-xxl"><?php echo e($term->name); ?></h1>
                    </div>
                    <div class="cont-prod">
                    <?php
                        $query = new WP_Query( 
                            array( 
                                'post_type' => "product",
                                'tax_query' => array(
                                            array(
                                    'taxonomy' => 'product_cat',
                                    'field'    => 'slug',
                                    'terms'    => array( $term->slug ),
                                    ),
                                )
                            ) 
                            );     
                        ?>
                        <?php if( $query->have_posts() ): ?> 
                        <?php
                            $count = $query->found_posts;
                        ?>
                        <div class="<?php if( wp_is_mobile() ): ?> prod-mobile <?php elseif($count > 4): ?>swiper-container slider-category <?php else: ?> swiper-no-nav <?php endif; ?>">
                            <div class="bg-products swiper-wrapper">
                            <?php while( $query->have_posts() ): ?> 
                                <?php $query->the_post(); ?>
                                <div class="box-single-prod swiper-slide"> 
                                    <div class="single-prod">
                                        <a href="<?php echo e(the_permalink()); ?>">
                                            <div>
                                                <?php echo e(the_post_thumbnail()); ?>

                                                <h3 class="title-l"><?php echo e(the_title()); ?></h3>
                                                <?php woocommerce_template_single_price(); ?>
                                                <div>
                                                <?php
                                                $prodID  = get_the_ID();
                                                ?> 
                                                
                                                


                                                
                                                </div>
                                            </div>
                                        </a>    
                                    </div>
                                    <div class="btn-add-to-cart">
                                        <?php echo woocommerce_template_single_add_to_cart(); ?>

                                    </div>
                                </div><!--box-single-prod-->
                            <?php wp_reset_postdata(); ?>
                            <?php endwhile; ?>
                      
                            </div><!--swiper-wrapper-->
                            <div class="navigation">
                                <div class="swiper-prev"><?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="13px" viewBox="0 0 30 13" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line 4</title>
    <desc>Created with Sketch.</desc>
    <g id="Shop-/-Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="Prodotti-4-Copy" transform="translate(-100.000000, -902.000000)" stroke="#250F02" stroke-width="3">
            <polyline id="Line-4" transform="translate(115.588879, 908.332891) scale(-1, -1) translate(-115.588879, -908.332891) " points="103.663445 912.678561 127.514312 912.678561 121.446498 903.987221"/>
        </g>
    </g>
</svg>'; ?></div>
                                <div class="swiper-next"><?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="29px" height="14px" viewBox="0 0 29 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line 4</title>
    <desc>Created with Sketch.</desc>
    <g id="Shop-/-Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="Prodotti-4-Copy" transform="translate(-1225.000000, -901.000000)" stroke="#250F02" stroke-width="3">
            <polyline id="Line-4" points="1226.51431 912.678561 1250.36518 912.678561 1244.29737 903.987221"/>
        </g>
    </g>
</svg>'; ?></div>
                            </div>
                        </div><!--swiper-container-->
                        <?php endif; ?>
                    </div>
                </div><!--single-cat-->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
        </div>
    <?php echo $__env->make('woocommerce.content.category-related', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('partials.components.return-top', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('partials.components.pre-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php endwhile; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/front-page.blade.php ENDPATH**/ ?>