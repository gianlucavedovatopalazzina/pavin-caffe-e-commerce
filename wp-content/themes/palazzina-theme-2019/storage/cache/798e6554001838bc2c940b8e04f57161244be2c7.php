<header class="woocommerce-products-header">
    <?php if(apply_filters('woocommerce_show_page_title', true)): ?>
      
    
	<?php
	global $wp_query;
    $cat = $wp_query->get_queried_object();
    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    $image = wp_get_attachment_url( $thumbnail_id ); 

    $frontID = get_option( 'page_on_front' );

	?>

	<div class="head-category" style="background-image: url(<?php echo e($image); ?>)">
		<h1 class="woocommerce-products-header__title page-title"><?php echo e(woocommerce_page_title()); ?></h1>
	</div>
      
    <?php endif; ?>

    <?php
      do_action('woocommerce_archive_description');
    ?>
</header><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/woocommerce/content/head.blade.php ENDPATH**/ ?>