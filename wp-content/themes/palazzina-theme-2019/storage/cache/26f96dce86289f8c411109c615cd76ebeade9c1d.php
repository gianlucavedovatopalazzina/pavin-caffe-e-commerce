<div class="content-archive">
    <div class="container">

    <?php
    $cate = get_queried_object();
    $cateID = $cate->term_id;

    $terms = get_terms( array(
        'taxonomy' => 'product_cat',
        'hide_empty' => false,
        'child_of' => $cateID, 
    ) );
    var_dump($terms);
    ?>

    <?php $__currentLoopData = $terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

    <?php
    $svg1 = get_field('svg_1', $term->term_taxonomy_id );
    $size = 'full'; 
    ?>

    <img src="<?php echo esc_url( $svg1['url']); ?>" alt="<?php echo esc_attr( $svg1['alt']); ?>" />


    <div class="single-cat" id="link-<?php echo e($term->slug); ?>">
        <div class="cont-tit">
            <h3 class="title-xs"><?php echo e($term->description); ?></h3>
            <h2 class="title-xxl"><?php echo e($term->name); ?></h1>
        </div>
        <div class="cont-prod">
            <?php
                $query = new WP_Query( 
                    array( 
                        'post_type' => "product",
                        'tax_query' => array(
                                    array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => array( $term->slug ),
                            ),
                        )
                    ) 
                );
            
            ?>

            <?php if( $query->have_posts() ): ?> 
            <?php
                $count = $query->found_posts;
            ?>
                <div class="<?php if($count > 4): ?>swiper-container  slider-category <?php elseif( wp_is_mobile() ): ?> swiper-container slider-mobile <?php endif; ?>">
                    <div class="bg-products swiper-wrapper">
                    <?php while( $query->have_posts() ): ?> 

                        <?php $query->the_post(); ?>
                        <div class="box-single-prod swiper-slide"> 
                            <div class="single-prod">
                                <?php echo e(the_post_thumbnail()); ?>

                                <h3 class="title-l"><?php echo e(the_title()); ?></h3>
                            
                            </div>
                        </div>
                  
                    <?php wp_reset_postdata(); ?>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div><!--swiper-wrapper-->
                <div class="navigation">
                    <div class="swiper-prev"><?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="13px" viewBox="0 0 30 13" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line 4</title>
    <desc>Created with Sketch.</desc>
    <g id="Shop-/-Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="Prodotti-4-Copy" transform="translate(-100.000000, -902.000000)" stroke="#250F02" stroke-width="3">
            <polyline id="Line-4" transform="translate(115.588879, 908.332891) scale(-1, -1) translate(-115.588879, -908.332891) " points="103.663445 912.678561 127.514312 912.678561 121.446498 903.987221"/>
        </g>
    </g>
</svg>'; ?></div>
                    <div class="swiper-next"><?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="29px" height="14px" viewBox="0 0 29 14" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Line 4</title>
    <desc>Created with Sketch.</desc>
    <g id="Shop-/-Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="Prodotti-4-Copy" transform="translate(-1225.000000, -901.000000)" stroke="#250F02" stroke-width="3">
            <polyline id="Line-4" points="1226.51431 912.678561 1250.36518 912.678561 1244.29737 903.987221"/>
        </g>
    </g>
</svg>'; ?></div>
                </div>
            </div><!--swiper-container-->
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/woocommerce/content-category.blade.php ENDPATH**/ ?>