<?php $__env->startSection('content'); ?>

<div class="head-page">
    <div class="container">
        <div>
            <div class="container-xs">
                <h1 class="title-xxl"><?php echo e(get_the_title( get_option('page_for_posts', true) )); ?></h1>
                

                <div id="menu-list-cat">
                    <div class="current">
                        
                    </div>
                    <div class="list">
                        
                    </div>
                </div> 
                
            </div>
        </div> 
    </div>
</div>

<div class="content-page-news content-news">
  
    <?php if( have_posts()): ?>
    <div class="container cont-news">
        <?php while( have_posts() ): ?> <?php the_post() ?>

        <?php echo $__env->make('partials.content.content-'.get_post_type(), \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
    <div>
         
    </div>
       <?php endwhile; ?>
    <?php endif; ?>
        
    </div>

    <?php echo $__env->make('partials.components.pagination', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
    <?php $__env->stopSection(); ?>

</div>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views//index.blade.php ENDPATH**/ ?>