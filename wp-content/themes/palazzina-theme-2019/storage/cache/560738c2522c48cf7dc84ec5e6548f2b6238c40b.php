<div class="return-top block-animation">
    <div class="container-l">
        <div class="flex justify-end" >
                <?= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="73px" height="74px" viewBox="0 0 73 74" version="1.1">
    <!-- Generator: Sketch 59 (86127) - https://sketch.com -->
    <title>Group 2</title>
    <desc>Created with Sketch.</desc>
    <g id="Shop-/-Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Prodotti-4-Copy" transform="translate(-1186.000000, -4569.000000)">
            <g id="Group-2" transform="translate(1186.053087, 4569.319005)">
                <circle id="Oval" fill="#250F02" cx="36.3990048" cy="36.3990048" r="36.3990048"/>
                <g id="left-arrow" transform="translate(36.399005, 35.399005) rotate(90.000000) translate(-36.399005, -35.399005) translate(17.399005, 19.399005)" fill="#FFFFFF" fill-rule="nonzero" stroke="#250F02" stroke-width="3">
                    <path d="M35.8639675,12.8534228 L35.9232846,12.8663984 L10.4954146,12.8663984 L18.4890081,4.85519512 C18.880439,4.46407317 19.0951545,3.93423577 19.0951545,3.37813821 C19.0951545,2.82204065 18.880439,2.29591057 18.4890081,1.90386179 L17.2452033,0.659439024 C16.8540813,0.268317073 16.3328943,0.0520569106 15.7771057,0.0520569106 C15.2210081,0.0520569106 14.6995122,0.266772358 14.3083902,0.657894309 L0.605837398,14.3592114 C0.213170732,14.751878 -0.00154471544,15.2749187 -8.27700478e-06,15.8313252 C-0.00154471544,16.3908211 0.213170732,16.9141707 0.605837398,17.3062195 L14.3083902,31.0087724 C14.6995122,31.3995854 15.2206992,31.6146098 15.7771057,31.6146098 C16.3328943,31.6146098 16.8540813,31.3992764 17.2452033,31.0087724 L18.4890081,29.7643496 C18.880439,29.3738455 19.0951545,28.8523496 19.0951545,28.296252 C19.0951545,27.7404634 18.880439,27.2464634 18.4890081,26.8556504 L10.4052033,18.7996504 L35.8923902,18.7996504 C37.0376423,18.7996504 37.9999917,17.8125772 37.9999917,16.6679431 L37.9999917,14.9078943 C37.9999917,13.7632602 37.0092195,12.8534228 35.8639675,12.8534228 Z" id="Path"/>
                </g>
            </g>
        </g>
    </g>
</svg>'; ?>
        </div>
    </div>
</div><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/partials/components/return-top.blade.php ENDPATH**/ ?>