<?php
 /*
*   Block Name: Content Macchinari home
*/  

if ( wp_is_mobile() ) {
    $posts_per_page = '2';
} else {
    $posts_per_page = '3';
}

?>

<div class="cover-black-light content-short-macchinari mob:cover-grey-light block-no-top">
<div class="container">
    <div>
        <div class="flex mob:flex-wrap">
            <div class="w-2/5 cover-red flex flex-col justify-center mob:w-full block-mob">
                <div class="container-xxs mob:py-20">
                    <h4 class="title-l"><?php pll_e('Ultime')?> <b><?php pll_e('macchine utensili')?></b> <?php pll_e('caricate')?></h4>
                    <p><?php pll_e('Hai anche tu un macchinario da vendere? Clicca qui sotto')?> </p>
                    <div class="pt-12"> <a href="/vendita-usato" class="btn-white"><?php pll_e('Vendi usato')?></a></div>
                   
                </div>
            </div>
            <div class="w-3/5 cover-white cont-macchinari mob:w-full block-mob">
                <div class="flex flex items-end">
                    <?php
                    $args = array(
                    'post_type' => 'macchinari',
        
                    'posts_per_page' => "$posts_per_page",
                  
                    );
                    
                $query = new WP_Query( $args );

                // Il Loop
                while ( $query->have_posts() ) :
                    $query->the_post();
                    ?>
                <?php echo $__env->make('partials.content.content-macchinari', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 

                <?php
                endwhile;

                // Ripristina Query & Post Data originali
                wp_reset_query();
                wp_reset_postdata(); 
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

</div><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/partials/content/content-short-macchinari.blade.php ENDPATH**/ ?>