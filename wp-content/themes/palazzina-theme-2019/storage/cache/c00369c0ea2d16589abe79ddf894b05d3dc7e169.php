<div class="pre-footer">
    <div class="container-l">
        <div class="flex items-center">
            <div class="flex">
                <?php if( have_rows('social', 'option') ): ?>
                <?php while( have_rows('social', 'option') ): ?> 
                    <?php the_row();
                    $image = get_sub_field('icona_social', 'option');
                    $link = get_sub_field('link_social', 'option');
                    ?>
                    <div class="single-image">
                    <a href="<?php echo e($link); ?>">
                            <img src="<?php echo e($image['url']); ?>" alt="<?php echo e($image['alt']); ?>" />
                        </a>
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <span></span>
            <div>
        <?php
            $images = get_field('icone_pagamenti', 'option');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
        ?>
        <?php if( $images ): ?>
            <div class="flex">
                <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image_id): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="single-image">
                        <?php echo e(wp_get_attachment_image( $image_id, $size )); ?>

                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>
            </div>
        </div>
    </div>
</div><?php /**PATH /Applications/MAMP/htdocs/pavinshop/wp-content/themes/palazzina-theme-2019/resources/views/partials/components/pre-footer.blade.php ENDPATH**/ ?>