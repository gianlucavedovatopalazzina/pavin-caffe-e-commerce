<?php

namespace Core;

trait ActivePlugin 
{   
    /**
     * All active plugin name
     * @var array
     */
    public $activePlugin = array();

    /**
     * Check if plugin is active
     * @return bool
     */
    public function isActive(string $pluginName)
    {
        return in_array($pluginName, $this->activePlugin);
    }

    /**
     * Set current active plugin 
     * @return void
     */
    public function setActivePlugin()
    {
        $activePlugins = get_option('active_plugins');
        
        foreach($activePlugins as $plugin) {
            $this->activePlugin [] = basename($plugin, ".php");;
        } 
    }

    /**
     * List of all active plugin
     * @return array
     */
    public function getActivePlugin()
    {
        return $this->activePlugin;
    }
}