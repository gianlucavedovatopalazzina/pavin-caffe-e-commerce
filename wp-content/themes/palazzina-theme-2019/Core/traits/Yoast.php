<?php

namespace Core;

trait Yoast
{
    /**
     * Get breadcrumb by yoast
     * @return void
     */
    public function getBreadcrumb()
    {
        return yoast_breadcrumb( '<div class="breadcrumbs"><p>','</p></div>' );
    }
}