<?php

namespace Core\Blade;

use DOMDocument;

trait Directive
{
    /**
     * Load svg in blade template
     * @param string $path
     */
    public static function svg ($path) {
        $path = ltrim(str_replace('"', "", $path), "/");
        
        $svg = new DOMDocument();
        $svg->load(get_template_directory_uri() . "/" . $path);
        $image = $svg->saveXML($svg->documentElement);

        return "<?= '$image'; ?>";
    }
}