<?php

namespace Core;

trait Post
{
    /**
     * Get post by id based on language
     * @param int $id
     * @return object
     */
    public function getPost(int $id, array $options = [])
    {
        return $this->isActive("polylang") ? 
            get_post(pll_get_post($id)) 
            : 
            get_post($id, $options);
    }

    /**
     * Get page link by id based on language
     * @param int $id
     * @return string|null
     */
    public function getPageLink(int $id)
    {
        return get_page_link($this->getPost($id));
    }

    /**
     * Get post link by id based on language
     * @param int $id
     * @return string|null
     */
    public function getPostLink(int $id)
    {
        return get_post_permalink($this->getPost($id));
    }

    /**
     * Get term link by id and taxonomy name
     * @param int $id
     * @param string $taxonomyName
     * @return string|null
     */
    public function getTermLink(int $id, string $taxonomyName)
    {
        return $this->isActive("polylang") ?
            get_term_link(get_term(pll_get_term($id), $taxonomyName)) 
            : 
            get_term_link(get_term($id, $taxonomyName)); 
    }
}