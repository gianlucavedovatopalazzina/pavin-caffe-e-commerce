<?php

namespace Core;

/**
 * Create all possible template file to search from
 * @param string|array $templates
 * @return array
 */
function filter_templates($templates)
{
    $paths = [
        'views',
        'resources/views'
    ];

    $bladeTemplates = array_map(function ($temp) use ($paths) {
        $temp = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($temp));

        $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

        /** Remove partial $paths from the beginning of template names */
        if (strpos($temp, '/')) {
            $temp = preg_replace($paths_pattern, '', $temp);
        }

        return array_map(function ($path) use ($temp) {
            return [
                "{$path}/{$temp}.blade.php",
                "{$path}/{$temp}.php",
                "{$temp}.blade.php",
                "{$temp}.php",
            ];
        }, $paths);

    }, $templates); 


    $ret = merge($bladeTemplates);
    
    return $ret;
}

function merge($arr)
{
    $ret = [];
    foreach($arr as $a) {
        $ret = array_merge($ret, $a);
    }

    if (is_array($ret[0])) {
        return merge($ret);
    }

    $ret = array_unique($ret);
    return $ret;
}

/**
 * Extract basename from file
 * @param string $path 
 * @return string
 */
function get_file_basename($path)
{
    return explode(".", basename($path))[0];
}

/**
 * Get root path
 * @return string
 */
function get_root_path()
{
    return dirname(get_theme_file_path());
}

/**
 * Get current blade views path
 * @return string 
 */
function get_blade_path()
{
    return get_root_path() . "/resources/views";
}

/**
 * Remove extenstion from file
 * @param string $path
 * @return string
 */
function remove_extension($path)
{
    return explode(".", $path, 2)[0];
}
