<?php

namespace Core;

use Core\App;
use Core\Acf;
use Core\Blade\Directive;
use Jenssegers\Blade\Blade as BladeEngine;

class Blade
{   
    use Directive;

    /**
     * The Blade instance.
     *
     * @var Jenssegers\Blade\Blade
     */
    protected static $blade;

    /**
     * Set construct private for singleton pattern
     */
    private function __construct()
    {}

    /**
     * Implementation of singleton pattern
     * @return Blade
     */
    public static function instance()
    {
        if (!self::$blade) {
            self::$blade = new BladeEngine(get_blade_path(), get_root_path() . '/storage/cache');
            
            self::directive();
        }

        return self::$blade;
    }

    /**
     * Extend blade with custom directive
     * @return php
     */
    public static function directive ()
    {
        self::$blade->directive("svg", array(self::class, "svg"));
    }

    /**
    * Output the correct template from wordpress hierarchy
    * @param string $path
    * @return null
    */
    public static function view ($path, $options = [])
    {   
        $blade_path = str_replace(get_blade_path(), "", $path);
        $blade_path = remove_extension($blade_path);

       $blade = self::instance();
       

       $bladeGlobals = [
            "app" => new App(),
            "Acf" => Acf::class,
       ];

        // [
        //     "acfOptions" => $block
        // ]

        foreach($options as $key => $option) {
            $bladeGlobals[$key] = $option;
        }

        echo $blade->make($blade_path, 
            $bladeGlobals
        );
    }
}