<?php

namespace Core;

/**
 * Template Hierarchy should search for .blade.php files
 * @param string $type
 */
array_map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
}, [
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
]);

/**
 * Render page using Blade
 * @param string $template
 * @return string
 */
add_filter('template_include', function ($template) {
    $path = str_replace(get_template_directory(), "", $template);

    if (locate_template($path)) {

        Blade::view($template);

        return get_template_directory() . '/index.php';
    }

    return $template;

}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 * @param string $comments_template
 * @return string
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $theme_template = locate_template(["views{$comments_template}", $comments_template]);

    if ($theme_template) {  
        Blade::view($theme_template);

        return get_template_directory() . '/index.php';
    }
    return $comments_template;
}, 100);


add_filter( 'show_admin_bar', function () {
 if (is_blog_admin()) {
    return true;
  }
  return false;
} );