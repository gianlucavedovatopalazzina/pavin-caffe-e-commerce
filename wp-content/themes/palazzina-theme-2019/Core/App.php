<?php

namespace Core;

class App
{
    use 
    ActivePlugin,
    Yoast,
    Post;
    

    public function __construct()
    {
        $this->setActivePlugin();
    }
    
    /**
     * Get current language
     * @return string
     */
    public function lang($lang = null)
    {
        if ($lang) {
            return $lang === \pll_current_language();
        }

        return get_bloginfo("language");
    }

    /**
     * Get site url
     * @return string
     */
    public function url()
    {
        return get_bloginfo("url");
    }

    /**
     * Get site charset
     * @return string
     */
    public function charset()
    {
        return get_bloginfo("charset");
    }

    /**
     * Get site name
     * @return string
     */
    public function name()
    {
        return get_bloginfo("name");
    }

    /**
     * Get privacy-policy url
     * @return string
     */
    public function privacyPolicyUrl()
    {
        return get_the_privacy_policy_link();
    }

    /**
     * Get site assets path 
     * or add a final value to the path
     * @param string $value
     * @return string
     */
    public function asset(string $value = "")
    {   
        return get_template_directory_uri() . '/' . ltrim($value, "/");
    }

    /**
     * Get body script
     * @return string
     */
    public function getBodyScript()
    {
        if (function_exists('get_body_script'))
            get_body_script();
    }

    /**
     * Get body class
     * @return string
     */
    public function getBodyClass()
    {
        return body_class();
    }

    /**
     * Translate string
     * @param string $string
     * @return string
     */
    public function translate($string, $print = true)
    {
        if($print) pll_e($string);
        
        return pll__($string);
    }

    public static function svg($path)
    {
        $path = ltrim($path, "/");

        $svg = new \DOMDocument();
        $svg->load(get_template_directory_uri() . "/" . $path);
        return $svg->saveXML($svg->documentElement);
    }
}