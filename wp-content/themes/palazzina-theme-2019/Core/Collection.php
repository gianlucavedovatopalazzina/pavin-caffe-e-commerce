<?php

namespace Core;

class Collection
{
    protected $collection = [];

    public function __construct($collection)
    {
        $this->collection = $collection;
        
        return $this;
    }

    public function map($fn)
    {
        return new self(array_map($fn, $this->collection));
    }

    public function filter($fn)
    {
        return new self(array_filter($this->collection, $fn));
    }

    public function all ()
    {
        return $this->collection;
    }

    public function pluck($index)
    {
        if (count($this->collection) -1 >= $index) 
            return $this->collection[$index];

        return null;
    }

    public function where($condition)
    {
        return array_filter($this->collection, function ($col) use ($condition) {
            $parts = is_string($condition) ? 
                explode("|", $condition)
                :
                $parts = $condition;

            $attr = $col->{$parts[0]};
            $operator = $parts[1];
            $to = $parts[2];

            return eval("return '$attr' $operator '$to';");
        });
    }

    public function isEmpty()
    {
        return (bool) !count($this->collection);
    }
}