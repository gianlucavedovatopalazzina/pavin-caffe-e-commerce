<?php 

namespace Core;

class Acf
{
    protected $basePath = "";
    protected $acf = array();

    public function __get($name)
    {
        $name = $this->$basePath . $name;

        if (!array_key_exists($name, $this->acf)) 
            $this->acf[$name] = get_field($name);

        return $this->acf[$name];
    }
}