<?php

namespace App\Helpers;

use Core\Collection;

class Media
{
    public static $items;

    public static function create(array $medias)
    {
        $c = [];

        foreach($medias as $media) {
            $obj = (object) $media;
            
            if (property_exists($obj, "low")) {
                $clonedObj = clone $obj;

                $clonedObj->name .= "-low";

                $c [] = $clonedObj;
            }
            
            $obj->low = false;

            $c [] = $obj;
        }

        self::$items = new Collection($c);


        self::$items->map(array(self::class, "addImageSize"));

        return self::get();
    }

    public static function get(string $case = "default")
    {
        switch($case)
        {
            case "high":
                return self::$items->filter(function ($media) {
                    return !$media->low;
                });
            case "low": 
                return self::$items->filter(function ($media) {
                    return $media->low;
                });
            default:
                return self::$items;
        }
    }

    public static function addImageSize($media)
    {
        add_image_size($media->name, $media->width, $media->height, $media->crop);

        return self::get();
    }
}


