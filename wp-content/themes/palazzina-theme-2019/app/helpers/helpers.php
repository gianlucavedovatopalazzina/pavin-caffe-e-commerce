<?php 

use Core\App;

/**
 * Print a value if exist 
 * or pass a default value
 * @param string $exist
 * @param string $string
 * @param string $default
 * @return string
 */
function printIfExists($exists, $string, $default = "")
{
    return (bool) $exists ? $string : $default;
}

function svg($path)
{
    return App::svg($path);
}

function strposa($needle, $haystack) {
    if(!is_array($needle)) $needle = array($needle);
    foreach($needle as $what) {
        if(($pos = strpos($haystack, $what))!==false) return $pos;
    }
    return false;
}