<?php

namespace App\Models;

class News
{
    /**
     * Total number of news
     * @var int
     */
    private static $totalNews;

    private static $lastNews = null;

    public static function getNewsWithout($arr)
    {
        $news = new \WP_Query(array(
            'post__not_in' => $arr,
            'post_type' => 'post',
            "paged" => get_query_var("paged") ?: 1
        ));

        self::$totalNews = $news->found_posts;
        return $news->posts;
    }

    /**
     * Paginate fetched product
     * @param array $options
     */
    public static function paginate()
    {
        return paginate_links( array(
            'base'               => '/news/%_%',
            'format'             => 'page/%#%',
            'total'              => (int) ceil(self::$totalNews / 12),
            'current'            => get_query_var("paged") ?: 1,
            'end_size'           => 4,
            'mid_size'           => 2,
            'prev_next'          => true,
            'prev_text'          => svg('/images/arrow-carousel.svg'),
            'next_text'          => svg('/images/arrow-carousel.svg'),
        ));
    }

    /**
     * Number of pages
     * @var int $totalPages
     */

    public static function getLastNews()
    {
        if (self::$lastNews) {
            return self::$lastNews;
        }

        self::$lastNews = (new \WP_Query(array(
            'limit' => 1,
            'post_type' => 'post',
        )))->posts[0];

        return self::$lastNews;
    }

    /**
     * Retrive total number Plastic materials from db
     */
    public static function getTotalsRecords()
    {
        global $wpdb;

        $query = "SELECT COUNT(*) FROM {$wpdb->prefix}" . self::$table;
        return (int) $wpdb->get_var($query);
    }

    /**
     * Get totals pages from pagination
     */
    public static function getTotalsPages()
    {
        if (!self::$totalPages) {
            self::$totalPages = (int) @ceil((int) self::getTotalsRecords() / self::$limit);
        }

        return self::$totalPages;
    }

    

    public static function save()
    {
        if (!current_user_can("manage_options")) die(); 
        
        $materials = self::getFromPosts([
            "brand",
            "polymer_family",
            "producer",
            "grade"
        ]);

        if (!empty(self::$errors)) {
            flashMessage("errors", self::$errors);
        }

        global $wpdb;

        $placeholder = "VALUES (" . implode(", ", array_fill(0, count($materials), "%s")) . ")";

        $query = "INSERT INTO $wpdb->prefix" . self::$table . 
            " ( ". implode(", ", array_keys($materials)) ." )"
            . " " .$placeholder;

        $wpdb->query(
            $wpdb->prepare($query, $materials)
        );

        flashMessage("success", "Materiale creato con successo");

        return wp_safe_redirect( wp_get_referer() );
    }

    public static function update()
    {
        if (!current_user_can("manage_options")) die(); 

        //SAVE ALL INPUT INSIDE DATABSE AND CHECK IF USER CAN EDIT THIS
    }

    public static function delete()
    {
        if (!current_user_can("manage_options")) die();

        $params = self::getFromPosts(["id"]);
        global $wpdb;

    
        $wpdb->query( 
            $wpdb->prepare( 
                "DELETE FROM $wpdb->prefix" . self::$table . 
                " WHERE id = %d"
                ,
                $params["id"]
            )
        );

        flashMessage("success", "Materiale eliminato con successo");

        return wp_safe_redirect( wp_get_referer() );
    }

    /**
     * Get all brand inside acf
     * correlated to plasticMaterials
     * @return array
     */
    public static function getBrand()
    {
        return get_field("brands", "option");
    }

    /**
     * Get all polymer type inside acf
     * correlated to plasticMaterials
     * @return array
     */
    public static function getType()
    {
        return get_field("polymers", "option");
    }

    /**
     * Get all producer type inside acf
     * correlated to plasticMaterials
     * @return array
     */
    public static function getProducer()
    {
        return get_field("producers", "option");
    }

    /**
     * Get all grade type inside acf
     * correlated to plasticMaterials
     * @return array
     */
    public static function getGrade()
    {
        return get_field("grades", "option");
    }

    public static function getFromPosts($names = [])
    {
        $filterdArray = [];

        foreach($names as $name) {
            if (!array_key_exists($name, $_POST)) {
                self::$errors[] = "Il campo {$name} è obbligatorio";
                continue;
            }

            $filterdArray[$name] = $_POST[$name];
        }

        return $filterdArray;
    }
}