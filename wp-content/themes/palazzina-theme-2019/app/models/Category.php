<?php

namespace App\Models;

class Category
{
    public static function getSubCategoryBy($categoryName, $childOf = null)
    {
        if (!$childOf) {
            $childOf  = get_queried_object()->term_id;
        }
        return get_terms( $categoryName, array( 'child_of' => $childOf) );
    }
}