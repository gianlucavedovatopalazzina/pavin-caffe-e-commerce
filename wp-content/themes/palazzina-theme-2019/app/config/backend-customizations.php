<?php

/**
 * Build custom widget with personal company info
 */
add_action('wp_dashboard_setup', function () {
    wp_add_dashboard_widget('custom_dashboard_widget', 'ASSISTENZA PalazzinaCreativa', function () { 
        echo 'Per info e assistenza chiamare <a href="tel:049 9302391">049 9302391</a><br>o scrivere a <a href="mailto:servizioclienti@palazzinacreativa.it">servizioclienti@palazzinacreativa.it</a>';
    });
});

/**
 * Add custom text inside footer
 */
add_filter('admin_footer_text', function () {
    echo 'Realizzato da <a target="_blank" href="https://www.palazzinacreativa.it/credits">PalazzinaCreativa</a>';
});

add_action('login_head', function () {
    echo '<style type="text/css">
    h1 a { background-image:url('.get_bloginfo('template_directory').'/img/logo-admin.png) !important; }
    body.login {
        background: #292929;
    }
    form#loginform {
        background: transparent;
        border: 0px;
        box-shadow: none;
    }
    .login label {
        color: #a0a0a0;
        font-weight: 100;
        font-size: 12px;
        letter-spacing: 1px;
        text-transform: uppercase;
    }
    .login #backtoblog a, .login #nav a {
        color: #a0a0a0;
        transition: .5s;
    }
    .login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
        color: #fff;
    }
    input#wp-submit {
        background: #4CAF50;
        border: 0px;
        box-shadow: none;
        border-radius: 0px;
        text-shadow: none;
        text-transform: uppercase;
        font-weight: bold;
        letter-spacing: 1px;
        padding: 0 30px;
        transition: .5s;
    }
    input#wp-submit:hover {
        background: #43ca49;
    }
    .login form .input, .login form input[type=checkbox], .login input[type=text] {
        font-weight: 400;
    }
    #loginform input:focus, 
    #loginform * {
        outline:none;
        border: 0px;
    }
    input[type=checkbox]:checked:before {
        color: #43ca49;
    }
</style>';
});

/**
 * Print custom style inside header
 */
add_action('wp_before_admin_bar_render', function () {
    echo '
    <style type="text/css">
    #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
    background-image: url(' . get_bloginfo('stylesheet_directory') . '/img/logo-admin.png) !important;
    background-position: center;
    color:rgba(0, 0, 0, 0);
    background-size: contain;
    background-repeat: no-repeat;
    }
    #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
    background-position: 0 0;
    }
    .acf-field.acf-field-repeater.acf-field-5d88e09a0e10b.-c0 .acf-table tbody {
        display: flex;
        flex-wrap: wrap;
    }
    .acf-field.acf-field-repeater.acf-field-5d88e09a0e10b.-c0 .acf-table tbody tr {
        width: 300px;
    }
    </style>';
});

/**
 * Remove dashboard widgets
 */
add_action('wp_dashboard_setup', function () {
    global $wp_meta_boxes;
    
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
} );