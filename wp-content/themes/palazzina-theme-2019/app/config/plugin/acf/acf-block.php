<?php
/*
|--------------------------------------------------------------------------
| ACF Block
|--------------------------------------------------------------------------
|
| 
|
*/

use Core\Blade;

add_action('acf/init', function () 
{

    // register a spacing block
    acf_register_block(array(
        'name'				=> 'spacing',
        'title'				=> __('Spacing'),
        'description'		=> __('Spa'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));

     // register a text emotional block
     acf_register_block(array(
        'name'				=> 'slide',
        'title'				=> __('Slide'),
        'description'		=> __('Slide'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));

     // register a text emotional block
     acf_register_block(array(
        'name'				=> 'block-testo-centrale-top',
        'title'				=> __('Block testo centrale top'),
        'description'		=> __('Block testo centrale top'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));

     // register a text emotional block
     acf_register_block(array(
        'name'				=> 'gruppo-sezioni-home',
        'title'				=> __('Gruppo sezioni home'),
        'description'		=> __('Gruppo sezioni home'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));

    // register a text emotional block
     acf_register_block(array(
        'name'				=> 'testo-introduttivo',
        'title'				=> __('Testo introduttivo'),
        'description'		=> __('Testo introduttivo'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));

    // register a text emotional block
     acf_register_block(array(
        'name'				=> 'gallery',
        'title'				=> __('Gallery con photoswype'),
        'description'		=> __('Gallery con photoswype'),
        'render_callback'	=> 'acfBlockRenderCallback',
        'category'			=> 'palazzinablock',
        'icon'				=> 'admin-comments',
        //'keywords'			=> array( 'testimonial', 'quote' ),
    ));
});

/**
 * 
 */
function acfBlockRenderCallback( $block ) 
{
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("views/partials/blocks/{$slug}.blade.php") ) ) {
        Blade::view("partials/blocks/{$slug}", ["option" => $block]);
	}
}





add_filter('block_categories', function ($categories, $post) {
    return array_merge(
		$categories,
		array(
			array(
                'slug' => 'palazzinablock',
                'title' => __( 'Palazzina block', 'palazzinatheme' ),
                'icon'  => 'admin-home',
			),
		)
	);
}, 10, 2);




/*

add_filter( 'upload_dir', 'my_custom_upload_directory' );

function my_custom_upload_directory( $param ) {
        
  $folder = '/pdf';

  $param['path'] = $param['path'] . $folder;
  $param['url'] = $param['url'] . $folder;
 

  return $param; 

}

*/
/* 
    Pdf Field in Download page add media in pdf folder in Uploads
*/

add_filter( 'acf/upload_prefilter/name=pdf_download', 'secure_upload_prefilter' );

function secure_upload_prefilter( $errors ) {

  add_filter( 'upload_dir', 'secure_upload_directory' );

  return $errors;

}

function secure_upload_directory( $param ) {
        
  $folder = '/pdf';

  $param['path'] = $param['path'] . $folder;
  $param['url'] = $param['url'] . $folder;

  return $param; 

}






/* 
    Pdf Field in Download page add media in pdf folder in Uploads
*/

add_filter( 'acf/upload_prefilter/name=scheda_tecnica_macchinario', 'secure_upload_prefilter_schede' );

function secure_upload_prefilter_schede( $errors ) {

  add_filter( 'upload_dir', 'secure_upload_directory_schede' );

  return $errors;

}

function secure_upload_directory_schede( $param ) {
        
  $folder = '/schede-tecniche';

  $param['path'] = $param['path'] . $folder;
  $param['url'] = $param['url'] . $folder;

  return $param; 

}