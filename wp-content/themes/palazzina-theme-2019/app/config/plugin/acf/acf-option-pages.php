<?php
/*
|--------------------------------------------------------------------------
| ACF Options page
|--------------------------------------------------------------------------
|
| 
|
*/


//option page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Opzioni generali',
		'menu_title'	=> 'Opzioni generali',
		'menu_slug' 	=> 'opzioni-generali',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Social',
		'menu_title'	=> 'Social',
		'parent_slug'	=> 'opzioni-generali',
	));
/* 

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'opzioni-generali',
	)); */
	
}


