<?php
/*
|--------------------------------------------------------------------------
| Polylang
|--------------------------------------------------------------------------
|  
| 
|
*/

if ( is_plugin_active( 'polylang/polylang.php' ) ) {
    /**
     * Chang form for works with polylang 
     * @param $html
    */
    add_filter( 'get_search_form', function ($html) {
        $html = '
            <form class="search" method="get" action="'.esc_url( home_url('/') ).'" role="search">
                <input type="search" class="live-search" name="s" placeholder="'.esc_attr__( '', 'slupy' ).'">'.( defined( 'ICL_LANGUAGE_CODE' ) ? '<input type="hidden" name="lang" value="'.esc_attr( ICL_LANGUAGE_CODE ).'"/>' : '' ).'<button type="submit" >Cerca</button>
            </form>';
        return $html;
    } );
}

//polylang traduzioni
$stringhe = array(
    'Consegna standard: Gratis* lu, 07.10. - gi, 10.10.',
    'Info su Metodi di Pagamento, Resi e Spedizioni',
    'Note Organolettiche',
    'Miscela di caffè arabica accuratamente selezionati, dalla forma e colore particolarmente omogenei.',
    'Crema',
    'Acidità',
    'Finezza',
    'Caffeina',
    'Corpo',
    'Dolcezza',
    'Omogeneità',
    'Leggi tutto',
    'Carrello',
    'Prodotti',
    'Prodotti Pavin Caffè',
    );

foreach($stringhe as $str){
    pll_register_string( 'palazzina_theme', $str);
}