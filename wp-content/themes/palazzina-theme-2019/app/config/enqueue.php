<?php

use Core\Blade;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    
   wp_enqueue_style('typekit', 'https://use.typekit.net/jnw6jqt.css', false, null);
    wp_enqueue_style('main.css', dirname(get_template_directory_uri()) . '/dist/css/main.css', false, null);

    wp_enqueue_script('mainjs', dirname(get_template_directory_uri()) . '/dist/js/main.js', [], null, true);


   // if ( !is_admin() ) wp_deregister_script('jquery');

   // if (is_single() && comments_open() && get_option('thread_comments')) {
    //    wp_enqueue_script('comment-reply');
//    }

    wp_localize_script( 'mainjs', 'rcmitaliana', array(
  //      "lang" => pll_current_language(),
        "url" => site_url(),
        "adminUrl" => admin_url(),
        "apiUrl" => get_rest_url()
    ) );

}, 100);


//remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
//remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Remove unused scripts
 */

 
array_map(function ($hook) {
    add_action($hook, function () {
        if ( !is_user_logged_in() ) {
       //     wp_deregister_script('wp-embed');
     //       wp_dequeue_script('wp-embed');
        }
    }, 100000);
}, ["wp_print_scripts", "wp_print_footer_scripts"]);

/**
 * Remove unused styles
 */

 
 
array_map(function ($hook) {
    add_action($hook, function () {
        if ( !is_user_logged_in() ) {
            wp_deregister_style('wp-block-library');
            wp_dequeue_style('wp-block-library');
        }
    }, 100000);
}, ["wp_print_styles", "wp_print_footer_scripts"]);
 