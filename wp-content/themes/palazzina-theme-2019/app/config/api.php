<?php 

add_action("rest_api_init", function () {
    register_rest_field("page", "images", array(
        "get_callback" => function ($page) {
            $sizes = get_intermediate_image_sizes();
            $images = [];
            foreach($sizes as $size) {
                $images[$size] = wp_get_attachment_image_src( get_post_thumbnail_id( $page["id"] ), $size); 
            }

            return $images;
        }
    ));
});


