<?php

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('', 'sage') . '</a>';
});

/**
 * Adjust the excerpt length
 * @param int $length 
 */
add_filter('excerpt_length', function ($length) {
    return 20;
});

/**
 * Remove pefix from archive title
 * @param string $title
 */
add_filter('get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }
  
    return $title;
} );



add_action('pre_get_posts', 'change_tax_num_of_posts' );
function change_tax_num_of_posts( $wp_query ) {  
    if( is_tax('cat_prodotti')) {
        $wp_query->set('posts_per_page', -1);
    }
}