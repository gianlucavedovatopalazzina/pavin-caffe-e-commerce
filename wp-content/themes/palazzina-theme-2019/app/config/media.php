<?php

use App\Helpers\Media;

Media::create([
    ]);

/**
 * Add maximun quality to jpeg
 */
add_filter('jpeg_quality', function($quality){  
    return 100;
});

add_action('added_post_meta', function ($meta_id, $attach_id, $meta_key, $attach_meta) {
    if ($meta_key !== '_wp_attachment_metadata') return;


    $post = get_post($attach_id);

    if ($post->post_mime_type == 'image/jpeg' && is_array($attach_meta['sizes'])) {

        $pathinfo = pathinfo($attach_meta['file']);
        $uploads = wp_upload_dir();
        $dir = $uploads['basedir'] . '/' . $pathinfo['dirname'];

        $txt = "";

        foreach ($attach_meta['sizes'] as $size => $value) {
            $image = $dir . '/' . $value['file'];

            $resource = imagecreatefromjpeg($image);

            if (strpos($size, "-low") !== false) {
                // $txt .= $size . "\n\r";
                $parts = explode(".", $value["file"]);
                $ext = last($parts);
                
                $image = $dir . '/' . $parts[0] . "-low" . ".$ext";

                imagejpeg($resource, $image, 1);
            } else {
                imagejpeg($resource, $image, 100);
            }

            imagedestroy($resource);
        }

        $myfile = fopen("$dir/newfile", "w") or die("Unable to open file!");
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}, 10, 4);


// enable svg files in media 

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');