const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

require('laravel-mix-tailwind');
require('laravel-mix-imagemin');
require('laravel-mix-purgecss');

mix.js('resources/js/main.js', '')
    .webpackConfig({
        output: {
            filename: "dist/js/[name].js",
            chunkFilename: "dist/js/[name]-bundled.js",
            publicPath: "/wp-content/themes/palazzina-theme-2019/"
        },
        resolve: {
            alias: {
                "jQuery": path.join(__dirname, "node_modules/jquery/dist/jquery.js")
            }
        },
        optimization: {
            splitChunks: {
                automaticNameDelimiter: "-"
            }
        }
    })
    .babelConfig({
        plugins: ['@babel/plugin-syntax-dynamic-import'],
    })
    .sass('resources/sass/main.scss', 'dist/css')
    // .sourceMaps(true, 'source-map')
    .options({
        processCssUrls: false,
        postCss: [ 
           tailwindcss('./tailwind.config.js'),
            // require('autoprefixer') 
        ],
        purifyCss: false, // Remove unused CSS selectors.
    })
    // .purgeCss()
    // .autoload({
    //      jquery: ['$', 'window.jQuery', 'jQuery'],
    // })
    .imagemin(
        { 
           from: "images/**.*", 
           to: "dist" 
        },
        {
            context: 'resources',
        },
        {
            optipng: {
                optimizationLevel: 5
            },
            jpegtran: null,
            plugins: [
                require('imagemin-mozjpeg')({
                    quality: 100,
                    progressive: true,
                }),
            ],
        }
    )
    .browserSync({
        proxy: 'http://pavinshop.local',
        port: 3000,
        files: [
            '/resources/sass/**/*.scss',
            '/resources/js/{*,**}.js'
        ]
    });
