export const getHeight = (target, className) => {
    let cont;

    if (!className) {
        cont = target;
    } else {
        cont = target.querySelector(className);
    }
    // console.log(cont);
    const cloned_cont = cont.cloneNode(true);
    cloned_cont.style.width = cont.clientWidth + 'px';
    cloned_cont.style.height = "auto";
        
    document.body.appendChild(cloned_cont);


    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(cloned_cont.clientHeight);
            document.body.removeChild(cloned_cont);
        }, 100)
    })
}

