
// import $ from "jquery";
// window.$ = window.jQuery = $;

import Router from './utils/Router';

import Home from "./scripts/Home";

import Pages from "./scripts/Pages";

import Termini from "./scripts/Termini";


import toggle from "./scripts/Toggle";

//Globals scripts
import globals from "./globals";

 window.toggle = toggle;

 toggle();

globals.forEach(cls => new cls());

(new Router([
    {
        attribute: ".swiper-container",
        instance: 'Swiper/Swiper.js',
        fn: ({default: Swiper}) => {
            Swiper.init();
        }
    },
    {
        attribute: ".photoswipe",
        instance: 'PhotoSwipe.js',
        fn: ({default: PhotoSwipe}) => {
        PhotoSwipe.initPhotoSwipeFromDOM(".photoswipe");
        }
    },

    {
        attribute: ".home",
        instance: Home
    },
    {
        attribute: ".header",
        instance: Pages
    },
    {
        attribute: ".single",
        instance: Single
    },
    {
        attribute: ".page-termini",
        instance: Termini
    }

]))
    .watch();



import Single from "./scripts/Single";
