

import {disableScroll, enableScroll} from './../helpers/disableScroll';
import { getHeight } from "./../helpers/getHeight";

const getParent = (el, parentClassname) => 
{   
    const classes = Array.from(el.parentElement.classList);
    if (classes.includes(parentClassname)) {
        return el.parentElement
    }

    return getParent(el.parentElement, parentClassname);
}

const handleLisClick = ({ target }) => 
{ 
    const button = target.closest('.toggle__box-btn');

    if(button.classList.contains('btn-disable-scroll')) {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
        disableScroll();
    }
    
    try {
        const toggle__box = getParent(button, 'toggle__box');
        toggle__box.classList.toggle('toggle__box--open');
    } catch(e)
    {
        console.log(e);
    }
   
}

const closeToggle = ({ target }) => 
{ 
    const button = target.closest('.toggle__box-close');
    window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
    });
    enableScroll();

    try {
        const toggle__box = getParent(button, 'toggle__box');
        toggle__box.classList.remove('toggle__box--open');
    } catch(e)
    {
        console.log(e);
    }
}





const slideToggleClick = ({ target }) => 
{ 
    const button = target.closest('.toggle__box-btn--slide_toggle');
    
    try {
        const toggle__box = getParent(button, 'toggle__box');
        
        const toggle__box_container = toggle__box.querySelector('.toggle__box-container');

        toggle__box.classList.toggle('toggle__box--open');


        if (toggle__box.classList.contains("toggle__box--open")) {
        let height;

        getHeight(toggle__box_container)
            .then(payload => {
            height = payload;
            toggle__box_container.style.height = `${ height }px`;
            button.classList.add('hidden');
            });

        return;
        }
        toggle__box_container.style.height = `0px`;

    } catch(e)
    {
        console.log(e);
    }



    // const box = target.closest('.toggle__box-btn--slide_toggle');
    // const cont = box.querySelector('.toggle__box-cont');

    // box.classList.toggle('toggle__box--open');
    // if (box.classList.contains("toggle__box--open")) {
    //   let height;
    //   getHeight(box, ".toggle__box-cont")
    //     .then(payload => {
    //       height = payload;
    //       cont.style.height = `${ height }px`;
    //     });

    //   return;
    // }
    // cont.style.height = `0px`;
   
}
export default () => 
{
    const btns_slide = document.querySelectorAll(".toggle__box-btn--slide_toggle");
    btns_slide.forEach(btn => btn.addEventListener("click", slideToggleClick));

    const btns = document.querySelectorAll(".toggle__box-btn");
    btns.forEach(btn => btn.addEventListener("click", handleLisClick));

    const closes = document.querySelectorAll(".toggle__box-close");
    closes.forEach(close => close.addEventListener("click", closeToggle));
}

 




