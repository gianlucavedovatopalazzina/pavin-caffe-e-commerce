class Pages
{
    constructor()
    {
        console.log("class Pages");

    // animation header 

        jQuery(window).scroll(function(){
            if (jQuery(this).scrollTop() > 150) {
                jQuery('.header').addClass('active');
            } else {
                jQuery('.header').removeClass('active');
            }
        });

    // animation pages  
    (function($) {   
        jQuery.fn.visible = function(partial) {
            var $t            = jQuery(this),
                $w            = jQuery(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;
          return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        }; 
      })(jQuery);
      
      jQuery(window).scroll(function(event) {




    // Block add class when is visible

        jQuery(".block-animation").each(function(i, el) {
          var el = jQuery(el);
          if (el.visible(true)) {
            el.addClass("active"); 
          } else {
            el.removeClass("active"); 
          }
        });

        jQuery(".img-left , .img-right").each(function(i, el) {
          var el = jQuery(el);
          if (el.visible(true)) {
            el.addClass("active"); 
          } else {
            // el.removeClass("active"); 
          }
        });
        
      });



// Page loaded

      jQuery(window).load(function () {
        jQuery(".load").addClass("loaded");
        jQuery("body").addClass("loaded");
    });



    // pavin code


    jQuery('.return-top').on("click",function(){
      jQuery('html, body').animate({
        scrollTop: 0
     }, 600);
    });


    jQuery( window ).scroll( function(){
      var scroll_pos =  jQuery(document).scrollTop();
      var calc_scroll_pos = scroll_pos * 0.19;
      var calc_scroll_pos2 = scroll_pos * 0.16;
      var calc_scroll_rot1 = scroll_pos * 0.09;
      var calc_scroll_rot2 = scroll_pos * 0.12;
      var calc_scroll_rot3 = scroll_pos * 0.14;
      jQuery('.parallax-animation1').attr('style','transform: translateY('+ calc_scroll_pos +'px) rotate('+ calc_scroll_rot1 +'deg); ; transition: transform 0s linear 0s; will-change: transform');
      jQuery('.parallax-animation2').attr('style','transform: translateY('+ calc_scroll_pos2 +'px) rotate('+ -calc_scroll_rot2 +'deg); transition: transform 0s linear 0s; will-change: transform');
      jQuery('.parallax-animation3').attr('style','transform: translateY('+ calc_scroll_pos2 +'px) rotate('+ -calc_scroll_rot3 +'deg); transition: transform 0s linear 0s; will-change: transform');

      //effetto scroll row
      jQuery('.fade-in-load-scroll').each(function() {
  
          var element = jQuery(this);
  
          setTimeout(function() {
              top_load_element(element);
          }, 250, element);
      });
  });




    }

}
export default Pages;