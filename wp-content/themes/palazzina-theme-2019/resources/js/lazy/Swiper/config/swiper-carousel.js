export const swiperCarousel = {
    elements: Array.from(document.querySelectorAll(".swiper-container.slider-category")),
    details: {
        speed: 500,
        // init: false,
        slidesPerView: 4,
        spaceBetween: 15,
        threshold:20,
        navigation: {
            nextEl: '.swiper-next',
            prevEl: '.swiper-prev',
        },
        // centerInsufficientSlides : true,
        // slideToClickedSlide: true,
        preventClicksPropagation: false,
        preventClicks: false,
        breakpoints: {
            767: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            769: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 20,
            },
          },
        on: {
            init() {
                if (this.slides.length < 5) {
                    document.querySelector(swiperCarousel.details.navigation.nextEl).style.display = "none";
                    document.querySelector(swiperCarousel.details.navigation.prevEl).style.display = "none";
                }
            },
 
        }
    }
}






export const swiperCarousel2 = {
    elements: Array.from(document.querySelectorAll(".swiper-container.slider-mobile")),
    details: {
        speed: 400,
        init: false,
        slidesPerView: 1,
        spaceBetween: 0,
        threshold:20,
        // navigation: {
        //     nextEl: '.swiper-next',
        //     prevEl: '.swiper-prev',
        // },
        on: {
            // init() {
            //     if (this.slides.length < 5) {
            //         document.querySelector(swiperCarousel.details.navigation.nextEl).style.display = "none";
            //         document.querySelector(swiperCarousel.details.navigation.prevEl).style.display = "none";
            //     }
            // }
        },
        breakpoints: {  
    
            // when window width is <= 480px     
            480: {       
               slidesPerView: 2,       
               spaceBetween: 20     
            },   
        
            // when window width is <= 640px     
            640: {       
               slidesPerView: 2,       
               spaceBetween: 30     
            } ,
            770: {
                slidesPerView: 3,   
                spaceBetween: 20,   
            }
        
         } 
    }
}