import Swiper from "swiper";

//Swiper configuration 
import { swiperCarousel } from "./config/swiper-carousel";
import { swiperCarousel2 } from "./config/swiper-carousel";

const configs = [
    swiperCarousel,
    swiperCarousel2,
]

/**
 * Init function for building all slides base on 
 * className and configuration details
 */
Swiper.init = () => {
    configs.forEach(({ elements, details }) => {
        elements.forEach(el => {
            const swiper = new Swiper(el, details)
            
            if(details.init == false) {
                swiper.init();
            }
        });
    });
}


export default Swiper;