<?php
namespace Core;

require_once dirname(dirname(__FILE__)) . "/vendor/autoload.php";

use Core\Framework;
use Jenssegers\Blade\Blade;

requireFiles([
    "../app/config/" => [
        'plugin/acf/acf-block', 
        'plugin/acf/acf-option-pages',
        'plugin/acf/acf-fiere',
        'plugin/woocommerce/action',
        // 'plugin/polylang',
        'backend-customizations', 
        'enqueue', 
        'filters', 
        'media', 
        'menu', 
        'sidebar', 
        'theme-support',
        'post-type',
        'api'
    ],

    "../Core/" => [
        "helpers", "filters", "woocommerce"
    ],
    "../app/helpers/" => [
        "helpers"
    ]
]);

/**
 * Require a files based on array key from path
 * and array values from filename
 * @param array $files
 */
function requireFiles($files)
{
    $paths = array_keys($files);

    foreach($paths as $path) {
        array_map(function ($file) use ($path) {
            $fileToRequire = $path . $file . ".php";
            locate_template($fileToRequire, true, true);
        }, $files[$path]);
    }
}