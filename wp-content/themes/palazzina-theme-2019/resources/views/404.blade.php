@extends('layouts.app')
<div class="header-space"></div>

@section('content')
	<div id="page_404">
		<div class="panel-grid-cell container-l">
			<h1>404</h1>
            <div class="">
                <h2>Ci dispiace ma questa pagina non esiste</h2>
                <p>Naviga nel nostro sito, siamo sicuri che troverai quello che stavi cercando.</p>
                <div class="torna_home">
                    <a href="{{ home_url() }}">Vai alla Home</a>
                </div>
            </div>
		</div>
	</div>
    <div class="spacing-m"></div>
@endsection