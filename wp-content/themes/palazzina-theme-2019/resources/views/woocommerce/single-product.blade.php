@php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
  global $post;
  $id = get_the_ID(); 

  $crema = get_field('crema', $id);
  $acidita = get_field('acidita', $id);
  $finezza = get_field('finezza', $id);
  $finezza = get_field('finezza', $id);
  $caffeina = get_field('caffeina', $id);
  $corpo = get_field('corpo', $id);
  $dolcezza = get_field('dolcezza', $id);
  $omogeneita = get_field('omogeneita', $id);

  $categories = get_the_terms( $post->ID, 'product_cat' );

@endphp

@extends('layouts.app')

@section('content')

<div class="content-single-product">
  <div class="spacing-m"></div>
  <div class="row-single-1">
    <div class="container">
      <div class="breadcrumb">
      {{-- @php woocommerce_breadcrumb(); @endphp --}}
      @foreach( $categories as $category ) 
      Shop /<a href="/#{{ $category->slug }}"> {{ $category->name }} / <span class="current">{{ the_title() }}</span>
      @endforeach
      </div>
    </div>
    <div class="container-xxl">
      <div class="svg-dec">
        @svg("/images/chicco1.svg")
      </div> 
    </div> 

    <div class="container">
      <div class="spacing-s"></div>
      @while ( have_posts() ) 
      @php the_post(); @endphp 
      <div class="cont-prod">
        {{-- <div class="svg-dec">@svg("/images/chicco1.svg")</div>  --}}
        <div class="flex flex-wrap">
          <div class="col-50">
      
          @php
          $image = get_field('immagine_interna', $id);
          @endphp 
          @if( $image ) 
          <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            @endif
          </div>
          <div class="col-50">
            @foreach( $categories as $category ) 
            <h2 class="title-xs uppercase">{{ $category->name }} </h2> 
      
            @endforeach

            <h1 class="title-xxl">{{ the_title() }}</h1>

            <div class="spacing-xs"></div>

            {{ the_content() }} 

            <div class="spacing-s"></div>

            <div class="price">{!!  woocommerce_template_single_price() !!}</div>

            <div class="spacing-s"></div>
            <div class="">
                {!!  woocommerce_template_single_add_to_cart() !!}
            </div>
          

      

            {{-- @php woocommerce_template_single_price() @endphp --}}
            @php

            @endphp
            <div class="spacing-m"></div>
            <div class="link-shipping-condition">
              <div class="flex">
                <div class="col-50">
                  <div class="flex">
                    <div class="cont-img">
                      <div>
                          @svg("/images/shipping.svg")
                      </div>

                    </div>
                    <div>
                      <div class="title-xs">@php pll_e('Consegna standard: Gratis* lu, 07.10. - gi, 10.10.'); @endphp</div>
                    </div>
                  </div>
                </div>
                <div class="col-50">
                    <div class="flex">
                      <div>
                          <div>
                            @svg("/images/pay.svg")
                          </div>
                      </div>
                      <div>
                        <div class="title-xs">@php pll_e('Info su Metodi di Pagamento, Resi e Spedizioni'); @endphp</div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          @php
           
          @endphp
        </div>
      </div>
    </div><!--container-->
  </div>
  <div class="spacing-xxl"></div>
  @if ( $crema || $acidita || $finezza || $caffeina || $corpo || $dolcezza || $omogeneita ) 
  <div class="row-single-2 ">
    <div class="container">
      <div>
        <h3 class="title-xxl text-center">@php pll_e('Note Organolettiche'); @endphp</h3>
        <div class="spacing-s"></div>
        <p class="text-center cont-550">@php pll_e('Miscela di caffè arabica accuratamente selezionati, dalla forma e colore particolarmente omogenei.'); @endphp </p>
      </div>
      <div class="svg-dec svg-right">@svg("/images/chicco1.svg")</div> 
      <div class="spacing-m"></div>
      <div class="">
        <div class="flex">
          <div class="col-50">

            @if ($crema) 
            <div class="flex single-note">
                <div class="title-l">@php pll_e('Crema'); @endphp </div>
                <div class="numb number-{{ $crema }}">
                     @include('partials.components.interval') 
                </div>
            </div>
            @endif

            @if ($acidita) 
            <div class="flex single-note">
              <div class="title-l">@php pll_e('Acidità'); @endphp </div>
              <div class="numb number-{{ $acidita }}">
                  @include('partials.components.interval') 
              </div>
            </div>
            @endif

            @if ($finezza) 
            <div class="flex single-note">
                <div class="title-l">@php pll_e('Finezza'); @endphp </div>
                <div class="numb number-{{ $finezza }}">
                    @include('partials.components.interval') 
                </div><!---->
            </div>
            @endif

            @if ($caffeina) 
            <div class="flex single-note">
                <div class="title-l"> @php pll_e('Caffeina'); @endphp</div>
                <div class="numb number-{{ $caffeina }}">
                    @include('partials.components.interval') 
                </div><!---->
            </div>
            @endif

          </div>
          <div class="col-50">
              @if ($corpo) 
              <div class="flex single-note">
                  <div class="title-l">@php pll_e('Corpo'); @endphp </div>
                  <div class="numb number-{{ $corpo }}">
                      @include('partials.components.interval') 
                  </div><!---->
              </div>
              @endif
  
              @if ($dolcezza) 
              <div class="flex single-note">
                  <div class="title-l">@php pll_e('Dolcezza'); @endphp </div>
                  <div class=" numb number-{{ $dolcezza }}">
                      @include('partials.components.interval') 
                  </div><!--crema-->
              </div>
              @endif
  
              @if ($omogeneita) 
              <div class="flex single-note">
                  <div class="title-l"> @php pll_e('Omogeneità'); @endphp</div>
                  <div class=" numb number-{{ $omogeneita }}">
                      @include('partials.components.interval') 
                  </div><!--crema-->
              </div>
              @endif

            </div><!--col-50-->
          </div><!--flex-->
        </div>
      </div>
    </div><!--row2-->
    @endif
    <div class="spacing-xxl"></div>
    <div class="row-single-3 social block-animation">
      <div class="container-xs">
        <div class="flex justify-around items-center">
          <div>
              <p class="title-l"><strong>Share this product: </strong></p>
          </div>
          <div>
              <?php echo do_shortcode('[addtoany]');?>
          </div>
        </div>
      </div>
    </div><!--row-single-3-->

    <div class="container-xxl">
        <div class="svg-dec">@svg("/images/chicco1.svg")</div> 
    </div>
    <div class="spacing-xl"></div>

 

    <div class="block-divider flex items-center row-single-3 block-animation">
      <span class="divider"></span>
      <div class="flex justify-center items-center mx-10">
 
          @svg("/images/dec.svg")
          @svg("/images/dec.svg")
          @svg("/images/dec.svg")
          {{-- @svg("/images/dec.svg")
          @svg("/images/dec.sv") g--}}
         
      </div>
      <span class="divider"></span>
    </div>

    <div class="spacing-xxl"></div>

    <div class="container-l">
        <div class="svg-dec svg-right">@svg("/images/chicco1.svg")</div> 
    </div>
  
    @include('woocommerce.content.related') 

    <div class="spacing-l"></div>

    <div class="container-xxl">
      <div class="svg-dec svg-left">@svg("/images/chicco1.svg")</div> 
    </div> 

    <div class="spacing-xxl"></div>

    @include('woocommerce.content.category-related')
  
    @include('partials.components.return-top')

    @include('partials.components.pre-footer')
  </div>

  @endwhile

</div>

@endsection