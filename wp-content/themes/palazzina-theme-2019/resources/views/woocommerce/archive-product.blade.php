@php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );

@endphp

@extends('layouts.app')

@section('content')

<div class="head-category" style="background-image: url({{ $thumb['0'] }})">
		<h1 class="woocommerce-products-header__title page-title">{{ woocommerce_page_title() }}</h1>
</div>

@include('woocommerce.content.content-category')

@include('partials.components.return-top')

@include('partials.components.pre-footer')

@endsection