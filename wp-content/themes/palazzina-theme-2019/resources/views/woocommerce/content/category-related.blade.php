@php
$categoryid = '16';
$categoryname = 'product_cat';
global $post;
@endphp

@if (is_front_page()) 
@php
$bloccocategoria = get_field('blocco_categoria_correlata', $categoryname . '_' . $categoryid );

$linkcateg = get_category_link( $categoryid );
@endphp

@elseif (is_singular( 'product' ))

@php
$categ = get_the_terms( $post->ID, 'product_cat' );

$categparentID = join(', ', wp_list_pluck($categ, 'parent'));

$bloccocategoria = get_field('blocco_categoria_correlata', $categoryname . '_' . $categparentID );

$linkcateg = get_category_link( $categparentID );

@endphp

@else 

@php

$term = get_queried_object();

// var_dump($term);

$categoryname = 'product_cat';

$bloccocategoria = get_field('blocco_categoria_correlata', $term );

$linkcateg = get_category_link( $term );

@endphp

@endif

@if ($bloccocategoria) 

<div class="category-related block-animation">
    <div class="container-l">
        <a href="{{ $linkcateg }}">
            <div class="block-overflow">
                <div class="cont-related-cat" style="background-image: url({{ esc_url( $bloccocategoria['cover_image']['url'] ) }})">
                    <div class="cont-text">
                        <h5 class="title-xs">{{ $bloccocategoria['sottotitolo-cat'] }}</h5>
                        <h4 class="title-xxl">{{ $bloccocategoria['titolo-cat'] }}</h4>
                        <p>{{ $bloccocategoria['testo-cat'] }}</p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
@endif