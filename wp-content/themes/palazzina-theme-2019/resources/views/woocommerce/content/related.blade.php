<div class="block-related">
    <div class="container">
        <div>
            <h3 class="title-xxl text-center block-animation">@php pll_e('Prodotti correlati'); @endphp</h3>
        </div>
        <div class="spacing-l"></div>
        <div>
                @php

            global $post;
            $terms = get_the_terms( $post->ID, 'product_cat' );
            foreach ($terms as $term) {
                $product_cat_id = $term->term_id;
                break;
}


                $query = new WP_Query( 
                    array( 
                        'post_type' => "product",
                        'tax_query' => array(
                                    array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => array( $term->slug ),
                            ),
                        )
                    ) 
                );
                @endphp
                @if ( $query->have_posts() ) 
                @php
                $count = $query->found_posts;
                @endphp


                <div class="block-animation @if ($count > 4)swiper-container  slider-category @elseif ( wp_is_mobile() ) swiper-container slider-mobile @endif">
                        <div class="bg-products swiper-wrapper">
                        @while ( $query->have_posts() ) 
    
                            @php $query->the_post(); @endphp
                            <div class="box-single-prod swiper-slide"> 
                                <div class="single-prod">
                                <a href="{{ the_permalink() }}">
                                    <div>
                                        <div class="flex justify-center">
                                                {{ the_post_thumbnail() }}
                                        </div>
                                        <h3 class="title-m text-center pt-10">{{ the_title() }}</h3>
                                        @php woocommerce_template_single_price(); @endphp
                                 
                                    </div>
                                </a>    
                                </div>
                            </div>
                      
                        @php wp_reset_postdata(); @endphp
    
                        @endwhile
                        @endif
                    </div><!--swiper-wrapper-->
                    <div class="navigation">
                        <div class="swiper-prev">@svg("/images/prev.svg")</div>
                        <div class="swiper-next">@svg("/images/next.svg")</div>
                    </div>
                </div><!--swiper-container-->
         
        </div>
    </div>
</div>