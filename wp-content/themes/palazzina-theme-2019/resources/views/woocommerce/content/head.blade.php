<header class="woocommerce-products-header">
    @if(apply_filters('woocommerce_show_page_title', true))
      
    {{-- {{ woocommerce_page_thumbnail() }} --}}
	@php
	global $wp_query;
    $cat = $wp_query->get_queried_object();
    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    $image = wp_get_attachment_url( $thumbnail_id ); 

    $frontID = get_option( 'page_on_front' );

	@endphp

	<div class="head-category" style="background-image: url({{ $image }})">
		<h1 class="woocommerce-products-header__title page-title">{{ woocommerce_page_title() }}</h1>
	</div>
      
    @endif

    @php
      do_action('woocommerce_archive_description');
    @endphp
</header>