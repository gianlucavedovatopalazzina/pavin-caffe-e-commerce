
@php 

$cateID = pll__('16');

@endphp
@extends('layouts.app')
@section('content')
    @while ( have_posts() ) @php the_post() @endphp
    <header class="woocommerce-products-header">
        <div class="head-category" style="background-image: url({{ get_the_post_thumbnail_url() }})">
            <h1 class="woocommerce-products-header__title page-title">{{ get_the_title() }}</h1>
        </div>
    </header>

    <div class="content-archive">
            <div class="container">
            @php
    
            $terms = get_terms( array(
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
                'child_of' => $cateID, 
            ) );
            // var_dump($terms);
            @endphp
        
            @foreach ($terms as $term) 
        
            @php
            //  var_dump($term);
            // $svg1 = get_field('svg_1', $term->term_taxonomy_id );

            $svg1 = get_field('svg_1', $term->taxonomy . '_' . $term->term_id );

            $svg2 = get_field('svg_2', $term->taxonomy . '_' . $term->term_id );
            @endphp
            <div class="block-cat block-animation">

       
                @if( $svg1 ) 
                
                <div class="img-left">
                    <img class="parallax-animation1" src="{{ $svg1['url'] }}">
                </div>
                @endif

                @if( $svg2 ) 
                <div class="img-right">
                    <img class="parallax-animation2" src="{{ $svg2['url'] }}">
                </div>
                @endif
            
                    <div class="single-cat " id="link-{{ $term->slug }}">
                        <div class="cont-tit">
                            <h3 class="title-xs">{{ $term->description  }}</h3>
                            <h2 class="title-xxl">{{ $term->name  }}</h1>
                        </div>
                        <div class="cont-prod">
                        @php
                            $query = new WP_Query( 
                                array( 
                                    'post_type' => "product",
                                    'tax_query' => array(
                                                array(
                                        'taxonomy' => 'product_cat',
                                        'field'    => 'slug',
                                        'terms'    => array( $term->slug ),
                                        ),
                                    )
                                ) 
                                );     
                            @endphp
                            @if ( $query->have_posts() ) 
                            @php
                                $count = $query->found_posts;
                            @endphp
                            <div class="@if( wp_is_mobile() ) prod-mobile @elseif ($count > 4)swiper-container slider-category @else swiper-no-nav @endif">
                                <div class="bg-products swiper-wrapper">
                                @while ( $query->have_posts() ) 
                                    @php $query->the_post(); @endphp
                                    <div class="box-single-prod swiper-slide"> 
                                        <div class="single-prod">
                                            <a href="{{ the_permalink() }}">
                                                <div>
                                                    {{ the_post_thumbnail() }}
                                                    <h3 class="title-l">{{ the_title() }}</h3>
                                                    @php woocommerce_template_single_price(); @endphp
                                                    <div>
                                                    @php
                                                    $prodID  = get_the_ID();
                                                    @endphp 
                                                    {{-- {!! do_shortcode('[add_to_cart id=""]') !!}  --}}
                                                    {{-- {!!  woocommerce_loop_add_to_cart_link() !!} --}}


                                                    {{-- {!!  woocommerce_template_single_add_to_cart() !!} --}}
                                                    </div>
                                                </div>
                                            </a>    
                                        </div>
                                        <div class="btn-add-to-cart">
                                            {!!  woocommerce_template_single_add_to_cart() !!}
                                        </div>
                                    </div><!--box-single-prod-->
                                @php wp_reset_postdata(); @endphp
                                @endwhile
                        
                                </div><!--swiper-wrapper-->
                                <div class="navigation">
                                    <div class="swiper-prev">@svg("/images/prev.svg")</div>
                                    <div class="swiper-next">@svg("/images/next.svg")</div>
                                </div>
                            </div><!--swiper-container-->
                            @endif
                        </div>
                    </div><!--single-cat-->
                </div>
            @endforeach
          
            </div>
        </div>
    @include('woocommerce.content.category-related')

    @include('partials.components.return-top')

    @include('partials.components.pre-footer')

    @endwhile

@endsection