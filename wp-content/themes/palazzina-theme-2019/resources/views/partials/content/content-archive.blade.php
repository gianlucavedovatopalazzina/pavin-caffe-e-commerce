@php
$id = get_queried_object()->term_id;
$testointro =  get_field( "testo_introduttivo_pagina", $id);
@endphp

@extends('layouts.app')

@section('content')

<div class="head-page">
    <div class="container">
        <div>
            <div class="container-xs">
                <h1 class="title-xxl">{{ the_title() }}</h1>
                <div>{{ $testointro }} </div>  
                {{ wp_list_categories() }}
            </div>
        </div> 
    </div>
</div>

<div class="content-page-news content-news">
  
    @if ( have_posts())
        @include('partials.content.content-'.get_post_type())
    @endif
@endsection

</div>