

@php
$pagamentiID = pll__('116');
$spedizioniID = pll__('118');
$terminiID = pll__('114');

// excerpt 
$pageID = get_the_ID();
$page_object = get_page( $pageID );
$text = $page_object->post_content;
$start = strpos($text, '<p>');
$end = strpos($text, '</p>', $start);
$excerpt = substr($text, $start, $end-$start+3);

@endphp

<div class="page-main termini page-termini">
    <div class="page__content">
        <div>
        @if(!wp_is_mobile() ) 
            <div class="container-l">
                <div class="flex">
                    <div class="nav">
                        <div id="pagamenti">{{ get_the_title($pagamentiID)}}</div>
                        <div id="spedizioni">{{ get_the_title($spedizioniID)}}</div>
                        <div id="termini">{{ get_the_title($terminiID)}}</div>
                    </div>
                    <div class="cont-text">
                        <div class="text-pagamenti block-text">
                            <h2 class="title-xxl">{{ get_the_title($pagamentiID)}}</h2>

                            @php
                            $page_object = get_page( $pagamentiID );
                            echo $page_object->post_content;
                            @endphp

                        </div>
                        <div class="text-spedizioni block-text">
                            <h2 class="title-xxl">{{ get_the_title($spedizioniID)}}</h2>
                            @php
                            $page_object = get_page( $spedizioniID );
                            echo $page_object->post_content;
                            @endphp
                        </div>
                        <div class="text-termini block-text">
                            <h2 class="title-xxl">{{ get_the_title($terminiID)}}</h2>
                            @php
                            $page_object = get_page( $terminiID );
                            echo $page_object->post_content;
                            @endphp
                        </div>
                         
                    </div>
                </div>
            </div>
            @elseif (wp_is_mobile() ) 
            <div class="container-l mobile">
                <div class="">
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl">{{ get_the_title($pagamentiID)}}</h2>
                        <div class="excerpt">
                            @php
                            $page_object = get_page( $pagamentiID);
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            @endphp
                        </div>
                        <div class="discover">
                            @svg("/images/discover.svg")
                        </div>
                    </div>
                        <div class="full-text">
                            <div class="discover">
                                @svg("/images/totop.svg")
                            </div>
                            @php echo $text @endphp
                        </div>
                    </div>
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl">{{ get_the_title($spedizioniID)}}</h2>
                        <div class="excerpt">
                            @php
                            $page_object = get_page( $spedizioniID );
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            @endphp
                        </div>
                        <div class="discover">
                            @svg("/images/discover.svg")
                        </div>
                    </div>
            
             
                        <div class="full-text">
                            <div class="discover">
                                @svg("/images/totop.svg")
                            </div>
                            @php echo $text @endphp
                        </div>
                    </div>
                    <div class="block-terms-mobile">
                        <h2 class="title-xxl">{{ get_the_title($terminiID)}}</h2>
                        <div class="excerpt">
                            @php
                            $page_object = get_page( $terminiID );
                            $text = $page_object->post_content;
                            $start = strpos($text, '<p>');
                            $end = strpos($text, '</p>', $start);
                            $excerpt = substr($text, $start, $end-$start+2);
                            echo $excerpt;
                            @endphp
                         </div>
                         <div class="discover">
                                @svg("/images/discover.svg")
                            </div>
                        </div>

                         <div class="full-text">
                            <div class="discover">
                                @svg("/images/totop.svg")
                            </div>
                            @php echo $text @endphp
                         </div>
                    </div> 
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

