
@php
    use App\Models\News;

    $id = get_the_ID();
   
    $categories = get_the_category();
    $catName = $categories[0]->name;

    $cat_no_img = array(
	'Video',
);
@endphp

    <div class="w-full"> 
   
        <div class="single-news block block-mob">
         
                <div>


                    <div class="flex flex-row mob:flex-wrap">
                        <div class="w-300 mob:w-full">
                        <a href="{{ the_permalink() }}">
                                <div class="cover @if (in_array($catName, $cat_no_img, true)) video-cat @endif" style="background-image:url({{ get_the_post_thumbnail_url() }})">
                                </div>
                            </a>

                            @if(in_array($catName, $cat_no_img, true))
                            <div class="video">
                                <div>
                                    <span></span>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="w-4/6 cover-white mob:w-full">
                            <a href="{{ the_permalink() }}">
                                <div class="h-full max-300 mob:h-auto ">
                                    <div class="flex max-300 flex-col h-full mob:h-auto">
                                        <div class="date"> 
                                            <div class="container-s py-2 tab:pl-4">{{ the_date('d - m - Y') }}</div>
                                        </div>
                                        <div class="container-s m-auto tab:pl-4 mob:py-4">
                                            <div class="">
                                                <h3 class="title-s">{{ $catName }}</h3>  
                                                <h2 class="title-l">{{ the_title() }}</h2>
                                            </div>
                                            <div class="py-2">{{ the_excerpt() }}
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cover-red btn flex items-center justify-center">
                            <a href="{{ the_permalink() }}">@php pll_e('Leggi tutto')@endphp</a>
                        </div>
                    </div>  
                </div>
        
        </div>
 
    </div>