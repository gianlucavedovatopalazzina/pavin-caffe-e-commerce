<div class="pre-footer">
    <div class="container-l">
        <div class="flex items-center">
            <div class="flex">
                @if( have_rows('social', 'option') )
                @while ( have_rows('social', 'option') ) 
                    @php the_row();
                    $image = get_sub_field('icona_social', 'option');
                    $link = get_sub_field('link_social', 'option');
                    @endphp
                    <div class="single-image">
                    <a href="{{ $link }}">
                            <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" />
                        </a>
                    </div>
                @endwhile
                @endif
            </div>
            <span></span>
            <div>
        @php
            $images = get_field('icone_pagamenti', 'option');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
        @endphp
        @if( $images )
            <div class="flex">
                @foreach( $images as $image_id )
                    <div class="single-image">
                        {{ wp_get_attachment_image( $image_id, $size ) }}
                    </div>
                @endforeach
            </div>
        @endif
            </div>
        </div>
    </div>
</div>