@php
    $detect = new Mobile_Detect;
@endphp


<!DOCTYPE html>
<html lang="{{ $app->lang() }}">
<head>
    <meta charset="{{ $app->charset() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    {!! wp_head() !!}
</head>


<body {{ $app->getBodyClass() }} id="body">
    @php function_exists("get_body_script") ? get_body_script() : ""; @endphp

    {{ $app->getBodyScript() }}

    <div class="wrapper">
        @if ( !wp_is_mobile() )
        <header class="header">
            <div class="header__container flex justify-between">
                <div class="logo">
                    <a href="<?= site_url(); ?>">
                        @svg("/images/logo.svg")
                    </a>
                </div>
         
                <div class="header__menu">
 
                {!! wp_nav_menu(['theme_location' => 'menu_1']) !!}
                </div>

                <div class="header__pll">  
                    <div class="menu_cart">
                        <a href="/carrello">
                            <div class="flex flex-col">
                                <div>
                                    {{-- @svg("/images/cart.svg") --}}
                                @php echo do_shortcode("[woo_cart_but]"); @endphp
                                </div>
                                <div>
                                    @php pll_e('Carrello'); @endphp 
                                </div>
                            </div>
                        </a>
                    </div>              
                    <div class="menu_pll">
                        <div class="current" id="current_lang">
                            <div>{{ pll_current_language('name') }}</div>
                        </div>
                        <ul id="list_pll" class="list-mobile" >@php pll_the_languages(array('show_flags'=>0,'show_names'=>1, 'hide_current'=>1)); @endphp</ul>	
                    </div>
                </div><!--pll_menu-->
            </div><!--header__container-->
        </header>

        @else 

        <header class="header mobile">
            <div class="header__container flex justify-between">
                <div class="header__pll">  
                    <div class="menu_cart">
                        <a href="{{ wc_get_cart_url() }}">
                            <div class="flex flex-col">
                                <div>
                                    {{-- @svg("/images/cart.svg") --}}
                                @php echo do_shortcode("[woo_cart_but]"); @endphp
                                </div>
                            </div>
                        </a>
                    </div>              
                    {{-- <div class="menu_pll">
                        <div class="current" id="current_lang">
                            <div>{{ pll_current_language('name') }}</div>
                        </div>
                        <ul id="list_pll" class="list-mobile" >@php pll_the_languages(array('show_flags'=>0,'show_names'=>1, 'hide_current'=>1)); @endphp</ul>	
                    </div>
                </div><!--pll_menu--> --}}
               
                <div class="logo">
                    <a href="<?= site_url(); ?>">
                        @svg("/images/logo.svg")
                    </a>
                </div>
         
                <div class="header__menu">
                    <div class="hamb">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="menu-cont" style="background-image: url({{ get_template_directory_uri() }}/images/cover_menu.jpg)">
                        <p class="title-xxl text-center">@php pll_e('Prodotti Pavin Caffè'); @endphp </p>
                            {!! wp_nav_menu(['theme_location' => 'menu_1']) !!}
                    </div>
 
                </div>

            </div><!--header__container-->
        </header>

        @endif
      
     
  
        <main id="main">
