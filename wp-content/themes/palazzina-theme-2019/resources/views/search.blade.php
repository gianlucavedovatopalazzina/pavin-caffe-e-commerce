@extends('layouts.app')

@section('content')
	<div id="primary" class="content-area">

    	@if ( have_posts() )

			<header class="page-header">
				<h1 class="page-title">
                    @php _e( 'Search results for:', 'palazzinatheme' ) @endphp
				</h1>
				<div class="page-description">{{ get_search_query() }}</div>
			</header><!-- .page-header -->

			@while ( have_posts() ) @php the_post() @endphp

				@include('partials.content.content-excerpt')

			@endwhile;
		
			{!! the_posts_navigation() !!}

			{{-- If no content, include the "No posts found" template. --}} 
		@else 
			@include('partials.content.content-none')
		@endif;
	</div><!-- #primary -->
@endsection