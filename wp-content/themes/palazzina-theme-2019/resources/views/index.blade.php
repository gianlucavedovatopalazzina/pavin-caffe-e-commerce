
{{-- @include('partials.content.content-archive')--}}

@extends('layouts.app')

@section('content')

<div class="head-page">
    <div class="container">
        <div>
            <div class="container-xs">
                <h1 class="title-xxl">{{ get_the_title( get_option('page_for_posts', true) ) }}</h1>
                {{-- <div>{{ $testointro }} </div>  --}}

                <div id="menu-list-cat">
                    <div class="current">
                        {{-- <span>@php pll_e('Tutte le categorie')@endphp</span> --}}
                    </div>
                    <div class="list">
                        {{-- {{ wp_list_categories( array (
                            'exclude'  => '1',
                        )) }} --}}
                    </div>
                </div> 
                
            </div>
        </div> 
    </div>
</div>

<div class="content-page-news content-news">
  
    @if ( have_posts())
    <div class="container cont-news">
        @while ( have_posts() ) @php the_post() @endphp

        @include('partials.content.content-'.get_post_type()) 
    <div>
         
    </div>
       @endwhile
    @endif
        
    </div>

    @include('partials.components.pagination') 
    @endsection

</div>