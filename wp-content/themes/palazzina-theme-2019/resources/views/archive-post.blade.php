
{{-- @include('partials.content.content-archive')--}}
@php
$categories = get_the_category();
$category_id = $categories[0]->cat_ID;

$page_for_posts = get_option( 'page_for_posts' );

$testointro =  get_field( "testo_introduttivo_pagina", $page_for_posts);

@endphp

@extends('layouts.app')

@section('content')

<div class="head-page">
    <div class="container">
        <div>
            <div class="container-xs">
                <h1 class="title-xxl">{{ get_the_title( get_option('page_for_posts', true) ) }}</h1>
                <div>{{ $testointro }} </div> 

                <div id="menu-list-cat">
                    <div class="current">
                        <span>{{ single_cat_title() }} </span>
                    </div>
                    <div class="list">
                            {{ wp_list_categories( array (
                                'exclude'  => array (1, $category_id), 
                            )) }}
                    </div>
                </div> 
                
            </div>
        </div> 
    </div>
</div>

<div class="content-page-news content-news">
  
    @if ( have_posts())
    <div class="container cont-news">
        @while ( have_posts() ) @php the_post() @endphp

        @include('partials.content.content-'.get_post_type()) 
    <div>
         
    </div>
       @endwhile
    @endif
        
    </div>

    @include('partials.components.pagination') 
    @endsection

</div>